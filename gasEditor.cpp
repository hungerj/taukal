//  TauKal: A dive decompression plan software
//  Copyright (C) 2014  Joerg Hunger
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "gasEditor.h"

#include <cmath>
#include <algorithm>

#include <QGroupBox>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QGridLayout>
#include <QDoubleValidator>
#include <QLineEdit>
#include <QTableView>
#include <QLabel>
#include <QEvent>
#include <QApplication>
#include <QFormLayout>
#include <QCheckBox>

#ifdef TAUKALMOBILE
#include <QScroller>
#include <QScrollerProperties>
#include <QGuiApplication>
#include <QScreen>
#include <QDebug>
#endif

GasDelegate::GasDelegate(int mv, QObject* parent)
    : QItemDelegate(parent),
      maxValue(mv)
    {
    }
    
QWidget* GasDelegate::createEditor(QWidget* parent, 
                                  const QStyleOptionViewItem& , 
                                  const QModelIndex& ) const
    {
    QLineEdit* editor = new QLineEdit(parent);
    QIntValidator* validator = new QIntValidator(editor);
    validator->setRange(0, maxValue);
    editor->setValidator(validator);
    editor->setInputMethodHints(Qt::ImhFormattedNumbersOnly);
    QEvent event(QEvent::RequestSoftwareInputPanel);
    QApplication::sendEvent(editor, &event);
    return editor;
    }

void GasDelegate::setEditorData(QWidget* editor, const QModelIndex& index) const
    {
    QLineEdit* lineEdit = dynamic_cast<QLineEdit*>(editor);
    auto value = index.model()->data(index, Qt::DisplayRole);
    lineEdit->setText(value.toString());
    }
    
void GasDelegate::setModelData(QWidget* editor, 
                              QAbstractItemModel* model, 
                              const QModelIndex& index) const
    {
    QLineEdit* lineEdit = dynamic_cast<QLineEdit*>(editor);
    model->setData(index, QVariant(lineEdit->text()));
    }
    
GasListModel::GasListModel(QVector<Gas>& _gasList, 
                     QObject* parent)
    : QAbstractTableModel(parent),
      gasList(_gasList)
    {
    headers << tr("Active")
            << tr("O2")
            << tr("He")
            << tr("N2")
            << tr("MOD")
            << tr("Depth")
            << tr("RMV");
    }
    
int GasListModel::rowCount(const QModelIndex&) const
    {
    return gasList.size();
    }
    
int GasListModel::columnCount(const QModelIndex&) const
    {
    return headers.size();
    }
    
Qt::ItemFlags GasListModel::flags(const QModelIndex& index) const
    {
    if (index.isValid())
        {
        if (index.column() == OXYGEN ||
            index.column() == HELIUM ||
            index.column() == DEPTH ||
            index.column() == RMV )
            return Qt::ItemIsEnabled | Qt::ItemIsEditable;
        else if (index.column() == ACTIVE)
            return Qt::ItemIsUserCheckable | Qt::ItemIsEnabled;
        else
            return Qt::ItemIsEnabled;
            
        }
    return QAbstractTableModel::flags(index);
    }
    
QVariant GasListModel::data(const QModelIndex& index, int role) const
    {
    if (index.isValid())
        {
        if (role == Qt::DisplayRole)
            {
            if (index.column() == OXYGEN)
                return QVariant(round(gasList[index.row()].oxygen * 100));
            else if (index.column() == HELIUM)
                return QVariant(round(gasList[index.row()].helium * 100));
            else if (index.column() == NITROGEN)
                return QVariant(round(gasList[index.row()].nitrogen * 100));
            else if (index.column() == MOD)
                return QVariant(QString::number(gasList[index.row()].mod, 'f', 0));
            else if (index.column() == DEPTH)
                return QVariant(QString::number(gasList[index.row()].changeDepth, 'f', 0));
            else if (index.column() == RMV)
                return QVariant(QString::number(gasList[index.row()].rmv, 'f', 0));
            }
        else if (role == Qt::CheckStateRole)
            {
            if (index.column() == ACTIVE)
                return QVariant(gasList[index.row()].active);
            }
        }
    return QVariant();
    }
    
QVariant GasListModel::headerData(int section, Qt::Orientation orientation, 
                                int role) const
    {
    if (role == Qt::DisplayRole)
        {
        if (orientation == Qt::Horizontal)
            return QVariant(headers[section]);
        else
            return QVariant(section + 1);
        }
    return QVariant();
    }        
          
bool GasListModel::setData(const QModelIndex& index, const QVariant& value, 
                         int role)
    {
    if (index.isValid())
        {
        if (role == Qt::EditRole)
            {
            bool ok;
            qreal realValue = value.toReal(&ok);
            if (index.column() == OXYGEN && ok)
                gasList[index.row()].setOxygen(realValue / 100.0);
            else if (index.column() == HELIUM && ok)
                gasList[index.row()].setHelium(realValue / 100.0);
            else if (index.column() == DEPTH && ok)
                gasList[index.row()].setChangeDepth(realValue);
            else if (index.column() == RMV && ok)
                gasList[index.row()].rmv = realValue;
            else
                return false;
            gasList[index.row()].active = true;
            dataChanged(QAbstractTableModel::index(index.row(), 0), 
                    QAbstractTableModel::index(index.row(), headers.size() - 1));
            return true;
            }
        else if (role == Qt::CheckStateRole)
            {
            if (index.column() == ACTIVE)
                {
                gasList[index.row()].active = ! gasList[index.row()].active;
                dataChanged(index, index);
                return true;
                }
            }
        }
    return false;
    }
    

GasEditor::GasEditor(DecoSettings& _settings, DivePlan& _plan,
                   QWidget* parent, Qt::WindowFlags f)
    : QWidget(parent, f),
      settings(_settings),
      plan(_plan)
    {
    QVBoxLayout* widgetLayout = new QVBoxLayout(this);
    
    QDoubleValidator* po2Validator = new QDoubleValidator(0.1, 2.0, 1, this);

    QFormLayout* settingsLayout = new QFormLayout();
    widgetLayout->addLayout(settingsLayout);
    
    po2BottomEditor = new QLineEdit(this);
    po2BottomEditor->setValidator(po2Validator);
    settingsLayout->addRow(tr("Max Po2 Bottom [bar]"), po2BottomEditor);
    connect(po2BottomEditor, SIGNAL(textEdited(const QString&)),
            SLOT(po2BottomEdited(const QString&)));
    
    po2DecoEditor = new QLineEdit(this);
    po2DecoEditor->setValidator(po2Validator);
    settingsLayout->addRow(tr("Max Po2 Deco [bar]"), po2DecoEditor);
    connect(po2DecoEditor, SIGNAL(textEdited(const QString&)),
            SLOT(po2BottomEdited(const QString&)));
    
    GasDelegate* gasDelegate = new GasDelegate(100, this);
    GasDelegate* depthDelegate = new GasDelegate(200, this);
    
    QGroupBox* ocFrame = new QGroupBox(tr("Open Circuit Gas List"),this);
    QVBoxLayout* ocLayout = new QVBoxLayout(ocFrame);
    ocGasEditor = new QTableView(ocFrame);
    ocLayout->addWidget(ocGasEditor);
    ocGasModel = new GasListModel(plan.gasList, this);
    ocGasEditor->setModel(ocGasModel);
    ocGasEditor->setEditTriggers(QAbstractItemView::CurrentChanged);
    ocGasEditor->setItemDelegateForColumn(GasListModel::OXYGEN, gasDelegate);
    ocGasEditor->setItemDelegateForColumn(GasListModel::HELIUM, gasDelegate);
    ocGasEditor->setItemDelegateForColumn(GasListModel::DEPTH, depthDelegate);
    ocGasEditor->setItemDelegateForColumn(GasListModel::RMV, depthDelegate);
    ocGasEditor->resizeColumnsToContents();
    widgetLayout->addWidget(ocFrame);

    QGroupBox* ccFrame = new QGroupBox(tr("Closed Circuit Gas List"),this);
    QVBoxLayout* ccLayout = new QVBoxLayout(ccFrame);
    ccGasEditor = new QTableView(ccFrame);
    ccLayout->addWidget(ccGasEditor);
    ccGasModel = new GasListModel(plan.ccGas, this);
    ccGasEditor->setModel(ccGasModel);
    ccGasEditor->setEditTriggers(QAbstractItemView::CurrentChanged);
    ccGasEditor->setItemDelegateForColumn(GasListModel::OXYGEN, gasDelegate);
    ccGasEditor->setItemDelegateForColumn(GasListModel::HELIUM, gasDelegate);
    ccGasEditor->setItemDelegateForColumn(GasListModel::DEPTH, depthDelegate);
    ccGasEditor->setItemDelegateForColumn(GasListModel::RMV, depthDelegate);
    ccGasEditor->resizeColumnsToContents();
    widgetLayout->addWidget(ccFrame);
    
#ifdef TAUKALMOBILE
    connect(ocGasEditor, SIGNAL(clicked ( const QModelIndex&)),
            SLOT(gasActivated(const QModelIndex&)));
    connect(ccGasEditor, SIGNAL(clicked ( const QModelIndex&)),
            SLOT(gasActivated(const QModelIndex&)));

    QScrollerProperties sp;
     
    sp.setScrollMetric(QScrollerProperties::DragVelocitySmoothingFactor, 0.6);
    sp.setScrollMetric(QScrollerProperties::MinimumVelocity, 0.0);
    sp.setScrollMetric(QScrollerProperties::MaximumVelocity, 0.5);
    sp.setScrollMetric(QScrollerProperties::AcceleratingFlickMaximumTime, 0.4);
    sp.setScrollMetric(QScrollerProperties::AcceleratingFlickSpeedupFactor, 1.2);
    sp.setScrollMetric(QScrollerProperties::SnapPositionRatio, 0.2);
    sp.setScrollMetric(QScrollerProperties::MaximumClickThroughVelocity, 0);
    sp.setScrollMetric(QScrollerProperties::DragStartDistance, 0.001);
    sp.setScrollMetric(QScrollerProperties::MousePressEventDelay, 0.5);
     
    QScroller* scroller = QScroller::scroller(ccGasEditor);
    scroller->grabGesture(ccGasEditor, QScroller::LeftMouseButtonGesture);
    scroller->setScrollerProperties(sp);

    scroller = QScroller::scroller(ocGasEditor);
    scroller->grabGesture(ocGasEditor, QScroller::LeftMouseButtonGesture);
    scroller->setScrollerProperties(sp);
#endif

    setup();
    }
    
void GasEditor::setup()
    {
    po2BottomEditor->setText(QString::number(
                              Gas::maxPo2Bottom * settings.PaToBar, 'f', 1));
    po2DecoEditor->setText(QString::number(
                              Gas::maxPo2Deco * settings.PaToBar, 'f', 1));
    }
    
void GasEditor::po2BottomEdited(const QString& text)
    {
    bool ok;
    double value = text.toDouble(&ok);
    if (ok)
        Gas::maxPo2Bottom = value * DecoSettings::BarToPa;
    }
    
void GasEditor::po2DecoEdited(const QString& text)
    {
    bool ok;
    double value = text.toDouble(&ok);
    if (ok)
        Gas::maxPo2Deco = value * DecoSettings::BarToPa;
    }
    
void GasEditor::gasActivated(const QModelIndex& index)
    {
    bool state = index.model()->data(index, Qt::CheckStateRole).toBool();
    const_cast<QAbstractItemModel*>(index.model())->
                                    setData(index, QVariant(! state), 
                                            Qt::CheckStateRole);
    }
    
    
