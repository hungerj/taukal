//  TauKal: A dive decompression plan software
//  Copyright (C) 2014  Joerg Hunger
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef GASEDITOR_H
#define GASEDITOR_H

#include "diveData.h"

#include <QWidget>
#include <QAbstractTableModel>
#include <QItemDelegate>

class QLineEdit;
class QTableView;
class GasModel;

class GasDelegate : public QItemDelegate
    {
    Q_OBJECT
    
    public:
    
    GasDelegate(int maxValue, QObject* parent = 0);
    
    virtual QWidget* createEditor(QWidget* parent, 
                                  const QStyleOptionViewItem& option, 
                                  const QModelIndex& index) const;
    
    virtual void setEditorData(QWidget* editor, const QModelIndex& index) const;
    virtual void setModelData(QWidget* editor, 
                              QAbstractItemModel* model, 
                              const QModelIndex& index) const;
    
    private:
    int maxValue;
    };
    
class GasListModel : public QAbstractTableModel
    {
    Q_OBJECT
    public:
    
    typedef enum {ACTIVE, 
                  OXYGEN,
                  HELIUM,
                  NITROGEN,
                  MOD,
                  DEPTH,
                  RMV} HEADER;
    
    GasListModel(QVector<Gas>& gasList, QObject* parent = 0);
    virtual int rowCount(const QModelIndex& parent = QModelIndex()) const;
    virtual int columnCount(const QModelIndex& parent = QModelIndex()) const;
    virtual Qt::ItemFlags flags(const QModelIndex& index ) const;
    virtual QVariant data(const QModelIndex& index, 
                          int role = Qt::DisplayRole) const;
    virtual QVariant headerData(int section, Qt::Orientation orientation, 
                                int role = Qt::DisplayRole) const;
    virtual bool setData(const QModelIndex& index, const QVariant& value, 
                         int role = Qt::EditRole);
    
    private:
    QVector<Gas>& gasList;
    QStringList headers;
    };
    

class GasEditor : public QWidget
    {
    Q_OBJECT
    
    public:
    
    GasEditor(DecoSettings& settings, DivePlan& plan,
                   QWidget* parent = 0, Qt::WindowFlags f = 0);
    
    void setup();
    
    public slots:
    void po2BottomEdited(const QString&);
    void po2DecoEdited(const QString&);
    
    protected slots:
    void gasActivated(const QModelIndex& index);
    
    protected:
    DecoSettings& settings;
    DivePlan& plan;
    QLineEdit* po2BottomEditor;
    QLineEdit* po2DecoEditor;
    QTableView* ocGasEditor;
    QTableView* ccGasEditor;
    GasListModel* ocGasModel;
    GasListModel* ccGasModel;
    };
    
#endif
