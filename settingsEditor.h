//  TauKal: A dive decompression plan software
//  Copyright (C) 2014  Joerg Hunger
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef SETTINGSEDITOR_H
#define SETTINGSEDITOR_H

#include "diveData.h"

#include <QWidget>
#include <QGroupBox>
#include <QItemDelegate>

class QTableView;
class QPushButton;
class QLineEdit;
class QRadioButton;
class QCheckBox;
class AscentRateModel;
class QFormLayout;

class DoubleDelegate : public QItemDelegate
    {
    Q_OBJECT
    
    public:
    
    DoubleDelegate(QObject* parent = 0);
    
    virtual QWidget* createEditor(QWidget* parent, 
                                  const QStyleOptionViewItem& option, 
                                  const QModelIndex& index) const;
    
    virtual void setEditorData(QWidget* editor, const QModelIndex& index) const;
    virtual void setModelData(QWidget* editor, 
                              QAbstractItemModel* model, 
                              const QModelIndex& index) const;
    
    private:
    };
    
class AscentRateEditor: public QGroupBox
    {
    Q_OBJECT
    
    public:
    
    AscentRateEditor(const QString& title, QVector<DiveRate>& rates, 
                     QWidget * parent = 0);
    
    protected:
    AscentRateModel* model;
    QTableView* view;
    QPushButton* addButton;
    QPushButton* removeButton;
    };
    
class SettingsEditor : public QWidget
    {
    Q_OBJECT
    
    public:
    
    SettingsEditor(DecoSettings& settings, 
                   QWidget* parent = 0, Qt::WindowFlags f = 0);
    
    signals:
    void decoTypeChanged(DecoSettings::DecoMode);
    
    public slots:
    void apply();
    void setup();
    void modeChanged(int mode);
    
    protected:
    QRadioButton* ocButton;
    QRadioButton* ccButton;
    QLineEdit* gfLowEditor;
    QLineEdit* gfHighEditor;
    QLineEdit* bailoutGfLowEditor;
    QLineEdit* bailoutGfHighEditor;
    QLineEdit* lastDecoDepthEditor;
    QLineEdit* decoDistanceEditor;
    QLineEdit* gasChangeEditor;
    QLineEdit* setPointEditor;
    QLineEdit* problemTimeEditor;
    QLineEdit* bailoutFactorEditor;
    QLineEdit* bailoutFactorTimeEditor;
    QLineEdit* surfacePressureEditor;
    QLineEdit* salinityEditor;
    QCheckBox* roundTimesCheckBox;
    QCheckBox* caveModeBox;
    QLineEdit* returnFactorEditor;
    QFormLayout* leftLayout;
    QFormLayout* rightLayout; 
    AscentRateEditor* ascentEditor;
    AscentRateEditor* descentEditor;
    
    DecoSettings& settings;
    };

class DesktopSettingsEditor : public  SettingsEditor
    {
    Q_OBJECT
    
    public:
    
    DesktopSettingsEditor(DecoSettings& settings, 
                   QWidget* parent = 0, Qt::WindowFlags f = 0);
    
    };
    
class PhoneSettingsEditor : public  SettingsEditor
    {
    Q_OBJECT
    
    public:
    
    PhoneSettingsEditor(DecoSettings& settings, 
                   QWidget* parent = 0, Qt::WindowFlags f = 0);
    
    };
    

#endif

    
