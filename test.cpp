#include "diveData.h"
#include "decoCalc.h"
#include "settingsEditor.h"
#include "divePlot.h"

#include <QApplication>

int main(int argc, char** argv)
    {
    DecoSettings settings;
    DivePlan plan(settings);
    DivePlan bailoutPlan(settings);
    QApplication app(argc, argv);
    SettingsEditor* editor = new SettingsEditor(settings);
    editor->show();
    
    DivePlanPlot* plot = new DivePlanPlot(plan, bailoutPlan);
    plot->show();
    plan.setDepth(0, 40.0);
    plan.appendSegment();
    plan.setDuration(1, 30.0 * 60.0);
    plan.appendSegment();

    for (auto gas : plan.gasList)
        printf("%s\n", gas.str().toLocal8Bit().data());
    doDecoCalc(settings, plan, false);
    for (auto segment : plan.userSegments)
        {
        printf("%s\n", segment.str().toLocal8Bit().data());
        }
    for (auto segment : plan.decoSegments)
        {
        printf("%s\n", segment.str().toLocal8Bit().data());
        }
    plot->plot(false);
    PressurePlot* pp = new PressurePlot(plan, bailoutPlan);
    pp->plot();
    pp->show();
    return app.exec();
    }
    

