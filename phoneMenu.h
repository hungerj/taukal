//  TauKal: A dive decompression plan software
//  Copyright (C) 2014  Joerg Hunger
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef PHONE_MENU_H
#define PHONE_MENU_H

#include <QWidget>
#include <QStringList>

class QStackedWidget;
class QLabel;
class QMenu;
class QPushButton;
class QSignalMapper;

class PhoneMenu : public QWidget
    {
    Q_OBJECT
    
    public:
    
    PhoneMenu(QWidget* parent = 0, Qt::WindowFlags f = 0);
    
    int addWidget(QWidget* widget, const QString& text);
    int addWidget(QWidget* widget, const QIcon& icon, const QString& text);
    
    public slots:
    void setCurrent(int index);
    
    protected slots:
    void menuClicked();
    
    protected:
    QStackedWidget* stackWidget;
    QLabel* menuLabel;
    QMenu* menu;
    QPushButton* menuButton;
    QStringList entries;
    QSignalMapper* mapper;
    };
    
#endif
