//  TauKal: A dive decompression plan software
//  Copyright (C) 2012  Joerg Hunger
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "decoCalc.h"
#include "diveData.h"
#include "decoOSTC.h"

#include <QVector>
#include <cmath>

void applySettings(const DecoSettings& settings,
                   DivePlan& plan,
                   DecoOSTC& deco,
                   bool bailout)
    {
    deco.setSurfacePressure(settings.surfacePressure * settings.PaToBar);
    deco.setLastDecoDepth(settings.lastStopDepth);
    deco.setDecoDistance(settings.decoStopDistance);
    if (bailout)
        {
        deco.setGradientFactors(settings.bailoutGfLow, 
                                 settings.bailoutGfHigh);
        }
    else
        deco.setGradientFactors(settings.gfLow, settings.gfHigh);
    
    if (bailout || settings.decoType == DecoSettings::OPEN_CIRCUIT)
        {
        deco.setSetPoint(0.0);
        setGases(plan.gasList, deco);
        deco.setDecoType(DecoOSTC::ZH_L16_GF_OC);
        }
    else if (settings.decoType == DecoSettings::CLOSED_CIRCUIT)
        {
        deco.setSetPoint(settings.setPoint);
        setGases(plan.ccGas, deco);
        deco.setDecoType(DecoOSTC:: ZH_L16_GF_CC);
        }
    }
    
void setGases(QVector<Gas>& gasList, DecoOSTC& deco)
    {
    for (int i = 0; i < gasList.size(); ++i)
        {
        if (gasList[i].active)
            deco.setGas(i, gasList[i].changeDepth, gasList[i].nitrogen, 
                         gasList[i].helium);
        }
    }
    
void doDecoCalc(const DecoSettings& settings,
                DivePlan& plan,
                bool bailout)
    {
    if (plan.planSegments.size() <= 1)
        return;
    qreal step = 2.0;
    DecoSettings::DecoMode decoMode = settings.decoType;
    DecoOSTC deco;
    deco.reset();
    applySettings(settings, plan, deco, false);
    qreal rmvFactor = 1.0;
    qreal bailoutRmvFactorEndTime = std::numeric_limits<qreal>::max();
    
    qreal userRunTime = plan.getUserRuntime();
    qreal bailoutRuntime = userRunTime + 1.0;
    if (settings.bailoutRuntime > 0)
        bailoutRuntime = settings.bailoutRuntime;
    qreal time = step;
    qreal firstStop = 0.0;
    qreal ceiling = 0.0;
    qreal pressure;
    DiveSegment* segment = plan.addDeco(plan.planSegments[0]);
    DiveSegment* planSegment = nullptr;
    qreal depth = segment->startDepth;
    qreal nextDepth = depth;
    qreal stopCounter = 0.0;
    bool gasChange = false;
    bool saveSegment = false;
    auto segmentType = DiveSegment::STOP;
    Gas* bestGas = nullptr;
    qreal caveTime = time;
    Gas* gas = &plan.getCurrentGas(time);
    deco.setCurrentGas(gas->index);
    bool endDive = false;

    while (not endDive)
        {
        deco.setDepth(depth);
        deco.nextDiveStep(time);
        int nStops = deco.getDecoStops();
        if (nStops > 0)
            {
            DecoStop stop = deco.getDecoStop(0);
            firstStop = stop.depth + settings.decoStopDistance;
            if (stop.depth != ceiling)
                {
                plan.ceilings.append(QPointF(time, ceiling));
                plan.ceilings.append(QPointF(time, stop.depth));
                ceiling = stop.depth;
                }
            }
        else
            {
            firstStop = 0.0;
            if (ceiling > 0.0)
                {
                plan.ceilings.append(QPointF(time, ceiling));
                plan.ceilings.append(QPointF(time, 0.0));
                ceiling = 0.0;
                }
            }
        
        if (std::fmod(time, TTS_INTERVAL) == 0.0)
            plan.ttsList.append(QPointF(time, deco.getAscentTime()));
        
        if (fmod(time, TISSUE_INTERVAL) == 0.0)    
            {
            qreal maxN2 = 0.0;
            qreal maxHe = 0.0;
            qreal maxTissue = 0.0;
            plan.pN2Tissues.append(Tissues());
            plan.pHeTissues.append(Tissues());
            for (int n = 0; n < N_TISSUES; ++n)
                {
                maxN2 = std::max(maxN2, deco.pTissueN2(n));
                maxHe = std::max(maxHe, deco.pTissueHe(n));
                maxTissue = std::max(maxTissue, deco.pTissueN2(n) + deco.pTissueHe(n));
                plan.pN2Tissues.back()[n] = deco.pTissueN2(n);
                plan.pHeTissues.back()[n] = deco.pTissueHe(n);
                }
            plan.pN2MaxTissue.append(maxN2);
            plan.pHeMaxTissue.append(maxHe);
            plan.pMaxTissue.append(maxTissue);
            }
        if (bailout and time >= bailoutRuntime)
            {
            decoMode = DecoSettings::OPEN_CIRCUIT;
            gas = &plan.getBestGas(depth);
            segment->gasIndex = gas->index;
            segment->setPoint = 0.0;
            segment->decoMode = decoMode;
            applySettings(settings, plan, deco, bailout);
            deco.setCurrentGas(gas->index);
            rmvFactor = settings.bailoutRmvFactor;
            if (settings.bailoutRmvFactorTime >= 0.)
                {
                bailoutRmvFactorEndTime = settings.bailoutRmvFactorTime + time;
                }
            if (settings.bailoutProblemTime > 0.0)
                stopCounter = settings.bailoutProblemTime;
            else
                saveSegment = true;
            segmentType = DiveSegment::PROBLEM;
            segment->type = segmentType;
            bailout = false;
            }
        if (time <= userRunTime)
            {
            auto result = plan.getDepth(time);
            nextDepth = result.get<0>();
            if (planSegment == nullptr)
                planSegment = result.get<1>();
            if (planSegment != result.get<1>()
                    or time == userRunTime)
                {
                saveSegment = true;
                planSegment = result.get<1>();
                }
            if (time <= bailoutRuntime)
                {
                gas = &plan.getCurrentGas(time);
                deco.setCurrentGas(gas->index);
                }
            stopCounter = 0.0;
            }
        else
            {
            if (stopCounter > 0.0)
                {
                stopCounter -= step;
                if (stopCounter <= 0.0)
                    {
                    saveSegment = true;
                    stopCounter = 0.0;
                    if (segmentType == DiveSegment::GAS_CHANGE)
                        {
                        gasChange = true;
                        segment->type = segmentType;
                        }
                    }
                nextDepth = depth;
                }
            else
                {
                if (settings.caveMode)
                    {
                    auto result = plan.getReverseDepth(caveTime);
                    nextDepth = result.get<0>();
                    if (planSegment == nullptr)
                        planSegment = result.get<1>();
                    if (planSegment != result.get<1>())
                        {
                        saveSegment = true;
                        planSegment = result.get<1>();
                        }
                    }
                else
                    {
                    qreal ascentRate = settings.getAscentRate(depth);
                    nextDepth =  depth - ascentRate * step;
                    }
                if (nextDepth > firstStop)
                    {
                    caveTime += step;

                    if (segmentType == DiveSegment::UNKNOWN)
                        {
                        segmentType = DiveSegment::ASCENT;
                        }
                    else if (segmentType != DiveSegment::ASCENT)
                        {
                        if (settings.roundTimes)
                            {
                            stopCounter = 60 - int(time) % 60;
                            }
                        else
                            {
                            saveSegment = true;
                            }
                        }
                    }
                else
                    {
                    nextDepth = firstStop;
                    if (segmentType == DiveSegment::UNKNOWN)
                        {
                        segmentType = DiveSegment::STOP;
                        segment->type = segmentType;
                        }
                    else if (segmentType != DiveSegment::STOP)
                        {
                        saveSegment = true;
                        segment->type = segmentType;
                        planSegment = nullptr;
                        }
                    }
                if (nextDepth < 0.1)
                    {
                    nextDepth = 0.0;
                    if (settings.roundTimes)
                        {
                        stopCounter = 60 - int(time) % 60;
                        }
                    }
                }
            }
        if (bestGas == nullptr
                and decoMode == DecoSettings::OPEN_CIRCUIT
                and depth > 0.1
                and time > bailoutRuntime)
            {
            bestGas = &plan.getBestGas(nextDepth);
            if (bestGas->index != gas->index)
                {
                if (bestGas->changeDepth > 0.0)
                    {
                    if (segment->startDepth > bestGas->changeDepth)
                        {
                        nextDepth = bestGas->changeDepth;
                        }
                    }
                saveSegment = true;
                if (settings.gasChangeDuration > 0.0
                        and planSegment == nullptr)
                    {
                    stopCounter = settings.gasChangeDuration;
                    segment->type = DiveSegment::GAS_CHANGE;
                    }
                else
                    gasChange = true;
                }
            else
                bestGas = nullptr;
            }

        if (time >= bailoutRmvFactorEndTime)
            rmvFactor = 1.0;
        if (decoMode == DecoSettings::OPEN_CIRCUIT)
            {
            pressure = settings.getPressure(depth);
            gas->consumption += pressure * gas->rmv * rmvFactor
                                * settings.PaToBar * step / 60.0;
            segment->consumption = gas->consumption;
            }

        if (saveSegment)
            {
            segment->consumption = gas->consumption;
            segment->gasIndex = gas->index;
            if (segmentType != DiveSegment::STOP)
                segment->endDepth = nextDepth;
            segment->duration = time - segment->startTime;
            segment->decoMode = decoMode;
            int delta = round((segment->endDepth - segment->startDepth) * 10);
            if (delta < 0)
                segment->type = DiveSegment::ASCENT;
            else if (delta > 0)
                segment->type = DiveSegment::DESCENT;
//            else
//                segment->type = segmentType;
            if (decoMode == DecoSettings::CLOSED_CIRCUIT)
                segment->setPoint = settings.setPoint;
            if (nextDepth > 0.0)
                {
                DiveSegment nextSegment = DiveSegment(time, segment->endDepth);
                segment = plan.addDeco(nextSegment, segmentType);
                segment->ceiling = ceiling;
                if (gasChange)
                    {
                    gas = bestGas;
                    deco.setCurrentGas(gas->index);
                    gasChange = false;
                    bestGas = nullptr;
                    segmentType = DiveSegment::UNKNOWN;
                    }
                else if (bestGas)
                    segmentType = DiveSegment::GAS_CHANGE;
                else
                    segmentType = DiveSegment::UNKNOWN;
                segment->gasIndex = gas->index;
                saveSegment = false;
                planSegment = nullptr;
                }
            else
                endDive = true;
            }

        time += step;
        depth = nextDepth;
        }
    }
    
   
