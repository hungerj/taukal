#include "decoOSTC.h"

#include <stdio.h>

extern "C"
{
#include "p2_definitions.h"
#include "shared_definitions.h"
// OSTC custom functions
unsigned short custom_functions[64];

}

#define DECO_SIM_SPEED 30
#define DECO_METER_TO_MBAR 98.066486
#define DECO_STD_ATM 1013.25
#define SIMULATION_TIME_STEP 2.0

DecoOSTC::DecoOSTC()
    : diveTime(0.0)
    {
    
    char_I_deco_model = 0;
    char_I_step_is_1min = 0;

    char_I_N2_ratio = 79;
    char_I_He_ratio = 0;
    char_I_deco_distance = 0;

    for (int i = 0; i < NUM_GAS; i++) {
        char_I_deco_gas_change[i] = 0;
        char_I_deco_N2_ratio[i] = 79;
        char_I_deco_He_ratio[i] = 0;
    }
    char_I_current_gas = 1;
    char_I_const_ppO2 = 0;
    
    char_I_GF_High_percentage = 90;
    char_I_GF_Low_percentage  = 30;
    
    char_I_saturation_multiplier = 110;
    char_I_desaturation_multiplier = 90;
    
    int_I_pres_respiration = DECO_STD_ATM;
    int_I_pres_surface = DECO_STD_ATM;
    reset();
    
    /* firmware 2.0 - deco_clear_tissue sets the variable to 0 */
    char_I_depth_last_deco = 3;
}

DecoOSTC::~DecoOSTC()
    {
    }

void DecoOSTC::reset()
    {
    /* clear CNS and tissues */
    int_I_pres_respiration = int_I_pres_surface;
    deco_clear_CNS_fraction();
    deco_clear_tissue();
    char_O_deco_status = 3;
    deco_calc_hauptroutine();
    diveTime = 0.0;
    }
        
void DecoOSTC::setSurfacePressure(qreal p_bar)
    {
    int_I_pres_surface = int(p_bar * 1000);
    }
    
void DecoOSTC::setDepth(qreal depth)
    {
    int_I_pres_respiration = depth * DECO_METER_TO_MBAR
            + int_I_pres_surface;
    }
    
void DecoOSTC::nextDiveStep(qreal time)
    {
    char_I_N2_ratio = char_I_deco_N2_ratio[char_I_current_gas - 1];
    char_I_He_ratio = char_I_deco_He_ratio[char_I_current_gas - 1];
    for ( ; diveTime < time; diveTime += SIMULATION_TIME_STEP)
        {
        deco_calc_hauptroutine();
        }
    }
    
qreal DecoOSTC::getNonDecoLimit()
    {
    qreal ndl = qreal(char_O_nullzeit);
    return ndl;
    }

qreal DecoOSTC::getAscentTime()
    {
    qreal tts = qreal(int_O_ascenttime);
    return tts;
    }
    
int DecoOSTC::getDecoStops()
    {
    decoStops.clear();
    for ( int i = 0; i < NUM_STOPS ; ++i)
        {
        if ( char_O_deco_depth[i] > 0)
            {
            DecoStop stop;
            stop.depth = char_O_deco_depth[i ] & 0x7f;
            stop.duration = char_O_deco_time[i];
            decoStops.push_back(stop);
            }
        else
            break;
        }
    return int(decoStops.size());
    }
    
DecoStop DecoOSTC::getDecoStop(int i)
    {
    return decoStops[i];
    }

Compartiment DecoOSTC::getCompartiment(int n)
    {
    Compartiment c;
    c.n2 = qreal(pres_tissue_N2[n]);
    c.he = qreal(pres_tissue_He[n]);
    return c;
    }

qreal DecoOSTC::pTissueN2(int n) const
    {
    return qreal(pres_tissue_N2[n]);
    }

qreal DecoOSTC::pTissueHe(int n) const
    {
    return qreal(pres_tissue_He[n]);
    }
    
void DecoOSTC::setGradientFactors(qreal low, qreal high)
    {
    char_I_GF_Low_percentage = (unsigned char)(low * 100.0);
    char_I_GF_High_percentage = (unsigned char)(high * 100.0);
    }
    
void DecoOSTC::setDecoType(DecoType type)
    {
    char_I_deco_model = (unsigned char)(type);
    }
    
void DecoOSTC::setGas(int n, qreal changeDepth, qreal n2_ratio, qreal he_ratio)
    {
    char_I_deco_gas_change[n] = (unsigned char)(changeDepth);
    char_I_deco_N2_ratio[n] = (unsigned char)(n2_ratio* 100.0);
    char_I_deco_He_ratio[n] = (unsigned char)(he_ratio* 100.0);
    }
    
void DecoOSTC::setSetPoint(qreal setPoint)
    {
    char_I_const_ppO2 = (unsigned char)(setPoint* 100.0);
    }

void DecoOSTC::setDecoDistance(qreal dist)
    {
    char_I_deco_distance = (unsigned char)(dist*10.0);
    }
    
void DecoOSTC::setLastDecoDepth(qreal depth)
    {
    char_I_depth_last_deco = (unsigned char)(depth);
    }
    
void DecoOSTC::setCurrentGas(int n)
    {
    char_I_current_gas = (unsigned char)(n+1);
    }
    
    
