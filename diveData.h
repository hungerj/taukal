//  TauKal: A dive decompression plan software
//  Copyright (C) 2014  Joerg Hunger
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef DIVEDATA_H
#define DIVEDATA_H

#include <QtGlobal>
#include <QString>
#include <QPointF>
#include <QVector>
#include <boost/tuple/tuple.hpp>
#include <array>

class QDomNode;
class QDomDocument;
class QDomElement;
class QTextCursor;
class QFile;

class Gas
    {
    public:
    static qreal maxPo2Bottom;
    static qreal maxPo2Deco;
    
    Gas();
    Gas(int i, bool a);
    
    void setOxygen(qreal);
    void setHelium(qreal);
    void setChangeDepth(qreal);
    
    void update();

    QString str() const;
    
    void save(QDomNode& root, QDomDocument& doc) const;
    void read(QDomElement& element);
    
    int index;
    bool active;
    qreal oxygen;
    qreal helium;
    qreal nitrogen;
    qreal mod;
    qreal changeDepth;
    qreal rmv;
    qreal consumption;
    
    };

class DiveRate
    {
    public:
    DiveRate()
        : depth(0),
          rate(10.0 / 60.0)
        {}
   
    DiveRate(double d, double r)
        : depth(d),
          rate(r)
        {}
    
    qreal depth;
    qreal rate;
    };


class DiveSample
    {
    public:
    DiveSample() 
        : time(0.0), depth(0.0)
        {}
    DiveSample(qreal t, qreal d)
        : time(t), depth(d)
        {}
        
    QPointF getPoint() const;
    qreal time;
    qreal depth;
    };

class DecoSettings
    {
    public:
    typedef enum {OPEN_CIRCUIT, CLOSED_CIRCUIT} DecoMode;
    qreal lastStopDepth;
    qreal decoStopDistance;
    qreal gfLow;
    qreal gfHigh;
    qreal bailoutGfLow;
    qreal bailoutGfHigh;
    DecoMode decoType;
    qreal setPoint;
    qreal gasChangeDuration;
    qreal bailoutProblemTime;
    qreal bailoutRmvFactor;
    qreal bailoutRmvFactorTime;
    qreal bailoutRuntime;
    qreal surfacePressure;
    qreal saltFactor;
    bool roundTimes;
    qreal returnFactor;
    bool caveMode;
    static const qreal MeterToPa;
    static const qreal PaToBar;
    static const qreal BarToPa;
    static const qreal PaToMBar;
    static const qreal MBarToPa;
    QVector<DiveRate> ascentRates;
    QVector<DiveRate> descentRates;
    
    DecoSettings();
    
    qreal getPressure(qreal depth) const;
    qreal getAscentRate(qreal depth) const;
    qreal getDescentRate(qreal depth) const;
    
    void save(QDomNode& root, QDomDocument& doc) const;
    void read(QDomNode& node);
    QString decoModeStr() const;
    
    private:
    template<class T, T DecoSettings::*ptr> 
        void setToElement(QDomNode& root, QDomDocument& doc, const QString& name) const;
            
    template<qreal DecoSettings::*ptr> 
        void setFromElement(QDomNode& root, const QString& name);
    template<bool DecoSettings::*ptr> 
        void setFromElement(QDomNode& root, const QString& name);

    };

class DiveSegment
    {
    public:
    typedef enum {DESCENT, ASCENT, STOP, PROBLEM, GAS_CHANGE, UNKNOWN} Type;
    
    DiveSegment();
    DiveSegment(const Gas& g, DecoSettings::DecoMode);
    DiveSegment(qreal startTime, qreal startDepth);
    DiveSegment(const QVector<DiveSample>& samples);
    
    qreal runtime()const;
    QString str() const;
    qreal getDepth(qreal time) const;
    
    qreal startTime;
    qreal duration;
    qreal startDepth;
    qreal endDepth;
    int gasIndex;
    qreal consumption;
    qreal setPoint;
    Type type;
    DecoSettings::DecoMode decoMode;
    qreal ceiling;
    };

const int N_TISSUES = 16;
const qreal TISSUE_INTERVAL = 10.0;
const qreal TTS_INTERVAL = 60.0;
typedef std::array<qreal,N_TISSUES> Tissues;

class DivePlan
    {
    public:
    
    QVector<Gas> gasList;
    QVector<Gas> ccGas;
    QVector<DiveSegment> planSegments;
    QVector<DiveSegment> decoSegments;
    QVector<QPointF> ceilings;
    QVector<QPointF> ttsList;
    QVector<Tissues> pN2Tissues;
    QVector<Tissues> pHeTissues;
    QVector<qreal> pN2MaxTissue;
    QVector<qreal> pHeMaxTissue;
    QVector<qreal> pMaxTissue;
    
    DivePlan(DecoSettings& ds);
    
    void clear();
    void clearDeco();
    void copy(const DivePlan&);
    
    void save(QDomNode& root, QDomDocument& doc) const;
    void read(QDomNode& node);
    void readSSRF(QDomNode& node);
    void createPlanFromSamples(qreal runtime = -1.0);
    void appendSegment();
    void removeLastSegment();
    void removeEmptySegments();
    void fixSegments();
    void setDepth(int i, qreal depth);
    void setDuration(int i, qreal duration);
    void decoTypeChanged(DecoSettings::DecoMode mode);
    DiveSegment* addDeco(DiveSegment& segment, DiveSegment::Type type = DiveSegment::STOP);
    
    void getUserDepthList(QVector<QPointF> list) const;
    void getDecoDepthList(QVector<QPointF> list) const;
    
    int getPlanSeriesSize() const;
    int getPlanSize(int n) const;
    QPointF getPlanData(int n, int i) const;
    const DiveSegment& getPlanSegment(int n, int i) const;
    const Gas& getPlanGas(int n) const;
    int getDecoSeriesSize() const;
    int getDecoSize(int n) const;
    QPointF getDecoData(int n, int i) const;
    const DiveSegment& getDecoSegment(int n, int i) const;
    const Gas& getDecoGas(int n) const;
    qreal getMaxDepth() const;
    qreal getMaxPressure() const;
    qreal getDepthForPressure(qreal pressure) const;
    
    boost::tuple<qreal, DiveSegment*> getDepth(qreal time);
    qreal getUserRuntime() const;
    qreal getDecoRuntime() const;
    boost::tuple<qreal, DiveSegment*> getReverseDepth(qreal time);
    int getGasSize() const;
    Gas& getGas(int);
    const Gas& getGas(int) const;
    Gas& getGas(int, DecoSettings::DecoMode);
    const Gas& getGas(int, DecoSettings::DecoMode) const;
    const Gas& getStartGas() const;
    Gas& getBestGas(qreal depth);
    Gas& getCurrentGas(qreal time);
    
    void getOxygenList(QVector<QPointF>& list) const;
    void getNitrogenList(QVector<QPointF>& list) const;
    void getHeliumList(QVector<QPointF>& list) const;
    void getNarcosisList(QVector<QPointF>& list) const;
    
    void createDivePlanDoc(QTextCursor& cursor) const;
    
    qreal deviationLimit;
    QVector<DiveSample> samples;
    qreal returnFactor;
    
    DecoSettings& settings;

    private:
    mutable QVector<int> planSeriesIndex;
    mutable QVector<int> decoSeriesIndex;
    };
    
QString timeLabel(qreal time);
    
#endif
  
