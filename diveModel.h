//  TauKal: A dive decompression plan software
//  Copyright (C) 2014  Joerg Hunger
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef DIVESEGMENTMODEL_H
#define DIVESEGMENTMODEL_H

#include "diveData.h"

#include <QAbstractTableModel>
#include <QItemDelegate>

class QComboBox;

class GasSegmentDelegate : public QItemDelegate
    {
    Q_OBJECT
    
    public:
    
    GasSegmentDelegate(DivePlan& plan, QObject* parent = 0);
    
    virtual QWidget* createEditor(QWidget* parent, 
                                  const QStyleOptionViewItem& option, 
                                  const QModelIndex& index) const;
    
    virtual void setEditorData(QWidget* editor, const QModelIndex& index) const;
    virtual void setModelData(QWidget* editor, 
                              QAbstractItemModel* model, 
                              const QModelIndex& index) const;
    
    private:
    DivePlan& plan;
    };
    
class DepthSegmentDelegate : public QItemDelegate
    {
    Q_OBJECT
    
    public:
    
    DepthSegmentDelegate(QObject* parent = 0);
    
    virtual QWidget* createEditor(QWidget* parent, 
                                  const QStyleOptionViewItem& option, 
                                  const QModelIndex& index) const;
    
    virtual void setEditorData(QWidget* editor, const QModelIndex& index) const;
    virtual void setModelData(QWidget* editor, 
                              QAbstractItemModel* model, 
                              const QModelIndex& index) const;
    
    private:
    };
    
class RuntimeSegmentDelegate : public QItemDelegate
    {
    Q_OBJECT
    
    public:
    
    RuntimeSegmentDelegate(QObject* parent = 0);
    
    virtual QWidget* createEditor(QWidget* parent, 
                                  const QStyleOptionViewItem& option, 
                                  const QModelIndex& index) const;
    
    virtual void setEditorData(QWidget* editor, const QModelIndex& index) const;
    virtual void setModelData(QWidget* editor, 
                              QAbstractItemModel* model, 
                              const QModelIndex& index) const;
    
    private:
    };
    
class DiveSegmentModel : public QAbstractTableModel
    {
    Q_OBJECT
    public:
    
    typedef enum {TYPE, 
                  RUNTIME,
                  DURATION,
                  END_DEPTH,
                  GAS,
                  CONSUMPTION,
                  START_DEPTH,
                  COUNT} HEADER;
    
    DiveSegmentModel(DivePlan& plan, 
                     QVector<DiveSegment>& segments, 
                     bool editable, 
                     QObject* parent = 0);
    
    public slots:
    void startUpdate();
    void endUpdate();
    
    virtual int rowCount(const QModelIndex& parent = QModelIndex()) const;
    virtual int columnCount(const QModelIndex& parent = QModelIndex()) const;
    virtual Qt::ItemFlags flags(const QModelIndex& index ) const;
    virtual QVariant data(const QModelIndex& index, 
                          int role = Qt::DisplayRole) const;
    virtual QVariant headerData(int section, Qt::Orientation orientation, 
                                int role = Qt::DisplayRole) const;
    virtual bool setData(const QModelIndex& index, const QVariant& value, 
                         int role = Qt::EditRole);
    private:
    DivePlan& plan;
    QVector<DiveSegment>& segments;
    QStringList headers;
    QStringList type2Str;
    bool editable;
    };

class AscentRateModel : public QAbstractTableModel
    {
    Q_OBJECT
    public:
    
    typedef enum {DEPTH, 
                  RATE} HEADER;
    
    AscentRateModel(QVector<DiveRate>& rates, QObject* parent = 0);
    virtual int rowCount(const QModelIndex& parent = QModelIndex()) const;
    virtual int columnCount(const QModelIndex& parent = QModelIndex()) const;
    virtual Qt::ItemFlags flags(const QModelIndex& index ) const;
    virtual QVariant data(const QModelIndex& index, 
                          int role = Qt::DisplayRole) const;
    virtual QVariant headerData(int section, Qt::Orientation orientation, 
                                int role = Qt::DisplayRole) const;
    virtual bool setData(const QModelIndex& index, const QVariant& value, 
                         int role = Qt::EditRole);
    
    public slots:
    void addLine();
    void removeLine();
    
    private:
    QVector<DiveRate>& rates;
    QStringList headers;
    };
    



#endif
