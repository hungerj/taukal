//  TauKal: A dive decompression plan software
//  Copyright (C) 2012  Joerg Hunger
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "diveData.h"

#include <cmath>
#include <algorithm>

#include <QObject>
#include <QDomDocument>
#include <QDomElement>
#include <QtXml>
#include <QTextCursor>
#include <QTextFormat>
#include <QMessageBox>

qreal Gas::maxPo2Bottom = 1.4 * 1e5;
qreal Gas::maxPo2Deco = 1.6 * 1e5;

Gas::Gas()
    : index(0),
      active(false),
      oxygen(0.21),
      helium(0.0),
      nitrogen(1.0 - oxygen - helium),
      mod(0.0),
      changeDepth(0.0),
      rmv(15.0),
      consumption(0.0)
    {
    }
    
Gas::Gas(int i, bool a)
    : index(i),
      active(a),
      oxygen(0.21),
      helium(0.0),
      nitrogen(1.0 - oxygen - helium),
      mod(0.0),
      changeDepth(0.0),
      rmv(15.0),
      consumption(0.0)
    {
    }
    
void Gas::setOxygen(qreal o2)
    {
    oxygen = o2;
    update();
    }
    
void Gas::setHelium(qreal he)
    {
    helium = he;
    update();
    }
    
void Gas::setChangeDepth(qreal depth)
    {
    changeDepth = depth;
    }
    
void Gas::update()
    {
    nitrogen = 1.0 - oxygen - helium;
    qreal maxPo2;
    if ( oxygen < 0.22 )
        maxPo2 = Gas::maxPo2Bottom;
    else
        maxPo2 = Gas::maxPo2Deco;
    mod = std::floor((maxPo2 / oxygen - DecoSettings::BarToPa) 
                      / DecoSettings::MeterToPa);
    if ( index > 0 )
        changeDepth = mod;
    }
    
QString Gas::str() const
    {
    return QString("%1/%2 (%3m)")
               .arg(round(oxygen * 100))
               .arg(round(helium * 100))
               .arg(round(changeDepth));
    }
    

void Gas::save(QDomNode& root, QDomDocument& doc) const
    {
    QLocale locale = QLocale::C;
    QDomElement gasElement = doc.createElement("Gas");
    gasElement.setAttribute("Active", active);
    gasElement.setAttribute("Oxygen", locale.toString(oxygen));
    gasElement.setAttribute("Helium", locale.toString(helium));
    gasElement.setAttribute("ChangeDepth", locale.toString(changeDepth));
    gasElement.setAttribute("Rmv", locale.toString(rmv));
    root.appendChild(gasElement);
    }
    
void Gas::read(QDomElement& element)
    {
    active = (element.attribute("Active") == "1");
    bool ok;
    qreal value;
    value = element.attribute("Oxygen").toDouble(&ok);
    if (ok)
        oxygen = value;
    value = element.attribute("Helium").toDouble(&ok);
    if (ok)
        helium = value;
    value = element.attribute("Rmv").toDouble(&ok);
    if (ok)
        rmv = value;
    update();
    value = element.attribute("ChangeDepth").toDouble(&ok);
    if (ok)
        changeDepth = value;
    }
    
QPointF DiveSample::getPoint() const
    {
    return QPointF(time, depth);
    }
    
DiveSegment::DiveSegment()
      : startTime(0.0),
        duration(0.0),
        startDepth(0.0),
        endDepth(0.0),
        gasIndex(0),
        consumption(0.0),
        setPoint(0.0),
        type(STOP),
        decoMode(DecoSettings::OPEN_CIRCUIT)
    {
    }
    
DiveSegment::DiveSegment(const Gas& gas, DecoSettings::DecoMode m)
      : startTime(0.0),
        duration(0.0),
        startDepth(0.0),
        endDepth(0.0),
        gasIndex(gas.index),
        consumption(0.0),
        setPoint(0.0),
        type(STOP),
        decoMode(m )
    {
    }
    
DiveSegment::DiveSegment(qreal st, qreal sd)
      : startTime(st),
        duration(0.0),
        startDepth(sd),
        endDepth(sd),
        gasIndex(0),
        consumption(0.0),
        setPoint(0.0),
        type(STOP),
        decoMode(DecoSettings::OPEN_CIRCUIT),
        ceiling(0)
    {
    }

DiveSegment::DiveSegment(const QVector<DiveSample>& samples)
      : gasIndex(0),
        consumption(0.0),
        setPoint(0.0),
        type(STOP),
        decoMode(DecoSettings::OPEN_CIRCUIT),
        ceiling(0)
    {
    int n = samples.size();
    if (n > 0)
        {
        startTime = samples[0].time;
        startDepth = samples[0].depth;
        duration = samples[n - 1].time - startTime;
        endDepth = samples[n - 1].depth;
        }
    }
    
    
qreal DiveSegment::runtime() const
    {
    return (startTime + duration);
    }
    
QString DiveSegment::str() const
    {
    QString s = QString("%1(%2) %3 -> %4 %5 %6 %7")
                       .arg(startTime, 0, 'f', 0)
                       .arg(duration, 0, 'f', 0)
                       .arg(startDepth, 0, 'f', 1)
                       .arg(endDepth, 0, 'f', 1)
                       .arg(gasIndex)
                       .arg(consumption, 0, 'f', 1)
                       .arg(ceiling, 0, 'f', 1);
    return s;
    }

qreal DiveSegment::getDepth(qreal time) const
    {
    qreal depth = (time - startTime) * (endDepth - startDepth) / duration + startDepth;
    return depth;
    }

const qreal DecoSettings::MeterToPa = 9806.6486;
const qreal DecoSettings::PaToBar = 1e-5;
const qreal DecoSettings::BarToPa = 1e5;
const qreal DecoSettings::PaToMBar = 1e-2;
const qreal DecoSettings::MBarToPa = 1e2;

DecoSettings::DecoSettings()
        : 
        lastStopDepth(3.0),
        decoStopDistance(1.0),
        gfLow(0.40),
        gfHigh(0.80),
        bailoutGfLow(0.9),
        bailoutGfHigh(0.9),
        decoType(DecoSettings::OPEN_CIRCUIT),
        setPoint(1.3),
        gasChangeDuration(60.0),
        bailoutProblemTime(0.0),
        bailoutRmvFactor(2.0),
        bailoutRmvFactorTime(-1.0),
        bailoutRuntime(-1.0),
        surfacePressure(101300.0),
        saltFactor(1.0),
        roundTimes(true),
        returnFactor(1.0),
        caveMode(false)
    {
    ascentRates << DiveRate(10, 0.08333333)
                << DiveRate(10, 0.16666666);
    descentRates << DiveRate(10, 0.33333333);
    }

qreal DecoSettings::getPressure(qreal depth) const
    {
    qreal pressure = surfacePressure + depth * MeterToPa * saltFactor;
    return pressure;
    }
    
qreal bisectRate(const QVector<DiveRate>& rates, qreal depth)
    {
    int l = 0;
    int m = rates.size() / 2;
    int r = rates.size() - 1;
    while (l != m && m != r)
        {
        if (rates[m].depth < depth)
            {
            l = m;
            m = (m + r) / 2;
            }
        else
            {
            r = m;
            m = (l + m) / 2;
            }
        }
    if (depth <= rates[l].depth)
        return rates[l].rate;
    else if (depth <= rates[m].depth)
        return rates[m].rate;
    else
        return rates[r].rate;
    }

qreal DecoSettings::getAscentRate(qreal depth) const
    {
    return bisectRate(ascentRates, depth);
    }

qreal DecoSettings::getDescentRate(qreal depth) const
    {
    return bisectRate(descentRates, depth);
    }

void DecoSettings::save(QDomNode& root, QDomDocument& doc) const
    {
    QLocale locale = QLocale::C;
    QDomElement settingsElement = doc.createElement("DecoSettings");
    
    if (decoType == OPEN_CIRCUIT)
        settingsElement.setAttribute("DecoMode", "OpenCircuit");
    else
        settingsElement.setAttribute("DecoMode", "ClosedCircuit");
    
    setToElement<qreal, &DecoSettings::lastStopDepth>(settingsElement, doc, "LastStopDepth");
    setToElement<qreal, &DecoSettings::decoStopDistance>(settingsElement, doc, "DecoStopDistance");
    setToElement<qreal, &DecoSettings::gfLow>(settingsElement, doc, "GradientFactorLow");
    setToElement<qreal, &DecoSettings::gfHigh>(settingsElement, doc, "GradientFactorHigh");
    setToElement<qreal, &DecoSettings::bailoutGfLow>(settingsElement, doc, "BailoutGradientFactorLow");
    setToElement<qreal, &DecoSettings::bailoutGfHigh>(settingsElement, doc, "BailoutGradientFactorHigh");
    setToElement<qreal, &DecoSettings::setPoint>(settingsElement, doc, "SetPoint");
    setToElement<qreal, &DecoSettings::gasChangeDuration>(settingsElement, doc, "GasChangeDuration");
    setToElement<qreal, &DecoSettings::bailoutProblemTime>(settingsElement, doc, "BailoutProblemTime");
    setToElement<qreal, &DecoSettings::bailoutRmvFactor>(settingsElement, doc, "BailoutRmvFactor");
    setToElement<qreal, &DecoSettings::bailoutRmvFactorTime>(settingsElement, doc, "BailoutRmvFactorTime");
    setToElement<qreal, &DecoSettings::surfacePressure>(settingsElement, doc, "SurfacePressure");
    setToElement<qreal, &DecoSettings::saltFactor>(settingsElement, doc, "SaltFactor");
    setToElement<bool, &DecoSettings::roundTimes>(settingsElement, doc, "RoundTimes");

    QDomElement ascentElement = doc.createElement("AscentRates");    
    for (const auto& rate: ascentRates)
        {
        QDomElement rateElement = doc.createElement("Rate");
        rateElement.setAttribute("Depth", locale.toString(rate.depth));
        rateElement.setAttribute("Value", locale.toString(rate.rate));
        ascentElement.appendChild(rateElement);
        }
    settingsElement.appendChild(ascentElement);
    
    QDomElement descentElement = doc.createElement("DescentRates");    
    for (const auto& rate: descentRates)
        {
        QDomElement rateElement = doc.createElement("Rate");
        rateElement.setAttribute("Depth", locale.toString(rate.depth));
        rateElement.setAttribute("Value", locale.toString(rate.rate));
        descentElement.appendChild(rateElement);
        }
    settingsElement.appendChild(descentElement);
    
    root.appendChild(settingsElement);
    }

void DecoSettings::read(QDomNode& root)
    {
    QDomElement settingsElement = root.firstChildElement("DecoSettings");

    if (settingsElement.attribute("DecoMode", "OpenCircuit") == "OpenCircuit")
        decoType = OPEN_CIRCUIT;
    else
        decoType = CLOSED_CIRCUIT;
    
    setFromElement<&DecoSettings::lastStopDepth>(settingsElement, "LastStopDepth");
    setFromElement<&DecoSettings::decoStopDistance>(settingsElement, "DecoStopDistance");
    setFromElement<&DecoSettings::gfLow>(settingsElement, "GradientFactorLow");
    setFromElement<&DecoSettings::gfHigh>(settingsElement, "GradientFactorHigh");
    setFromElement<&DecoSettings::bailoutGfLow>(settingsElement, "BailoutGradientFactorLow");
    setFromElement<&DecoSettings::bailoutGfHigh>(settingsElement, "BailoutGradientFactorHigh");
    setFromElement<&DecoSettings::setPoint>(settingsElement, "SetPoint");
    setFromElement<&DecoSettings::gasChangeDuration>(settingsElement, "GasChangeDuration");
    setFromElement<&DecoSettings::bailoutProblemTime>(settingsElement, "BailoutProblemTime");
    setFromElement<&DecoSettings::bailoutRmvFactor>(settingsElement, "BailoutRmvFactor");
    setFromElement<&DecoSettings::bailoutRmvFactorTime>(settingsElement, "BailoutRmvFactorTime");
    setFromElement<&DecoSettings::surfacePressure>(settingsElement, "SurfacePressure");
    setFromElement<&DecoSettings::saltFactor>(settingsElement, "SaltFactor");
    setFromElement<&DecoSettings::roundTimes>(settingsElement, "RoundTimes");

    ascentRates.clear();
    QDomElement ascentElement = settingsElement.firstChildElement("AscentRates");
    if (! ascentElement.isNull())
        {
        QDomElement element = ascentElement.firstChildElement("Rate");
        while (!element.isNull()) 
            {
            bool ok1, ok2;
            DiveRate rate(element.attribute("Depth").toDouble(&ok1),
                          element.attribute("Value").toDouble(&ok2));
            if (ok1 && ok2)
                ascentRates.append(rate);
            element = element.nextSiblingElement("Rate");
            }
        }
    
    descentRates.clear();
    QDomElement descentElement = settingsElement.firstChildElement("DescentRates");
    if (! descentElement.isNull())
        {
        QDomElement element = descentElement.firstChildElement("Rate");
        while (!element.isNull()) 
            {
            bool ok1, ok2;
            DiveRate rate(element.attribute("Depth").toDouble(&ok1),
                          element.attribute("Value").toDouble(&ok2));
            if (ok1 && ok2)
                descentRates.append(rate);
            element = element.nextSiblingElement("Rate");
            }
        }
    }
    
template<class T, T DecoSettings::*ptr> 
    void DecoSettings::setToElement(QDomNode& root, QDomDocument& doc, const QString& name) const
        {
        QLocale locale = QLocale::C;
        QDomElement element = doc.createElement(name);
        element.setAttribute("Value", locale.toString((*this).*ptr ));
        root.appendChild(element);
        }

template<qreal DecoSettings::*ptr> 
    void DecoSettings::setFromElement(QDomNode& root, const QString& name)
        {
        QDomElement element = root.firstChildElement(name);
        QString valueStr = element.attribute("Value");
        bool ok;
        qreal value = valueStr.toDouble(&ok);
        if (ok)
            (*this).*ptr = value;
        }

template<bool DecoSettings::*ptr> 
    void DecoSettings::setFromElement(QDomNode& root, const QString& name)
        {
        QDomElement element = root.firstChildElement(name);
        QString valueStr = element.attribute("Value");
        (*this).*ptr = (valueStr == "1");
        }

QString DecoSettings::decoModeStr() const
    {
    if (decoType == OPEN_CIRCUIT)
        return "OpenCircuit";
    else
        return "ClosedCircuit";
    }
    
DivePlan::DivePlan(DecoSettings& ds)
    : deviationLimit(0.5),
      settings(ds)
    {
    for (int i = 0; i < 5; ++i)
        gasList.append(Gas(i, (i == 0)));
    ccGas.append(Gas(0, true));
    planSegments<< DiveSegment(getGas(0), settings.decoType);
    }
    
void DivePlan::clear()
    {
    planSegments.clear();
    planSegments<< DiveSegment(getGas(0), settings.decoType);
    decoSegments.clear();
    decoTypeChanged(settings.decoType);
    ceilings.clear();
    ttsList.clear();
    planSeriesIndex.clear();
    getPlanSeriesSize();
    decoSeriesIndex.clear();
    pN2Tissues.clear();
    pHeTissues.clear();
    pN2MaxTissue.clear();
    pHeMaxTissue.clear();
    pMaxTissue.clear();
    samples.clear();
    }
    
void DivePlan::clearDeco()
    {
    for (auto& segment : planSegments)
        {
        segment.consumption = 0.0;
        }
    for (auto& gas : gasList)
        {
        gas.consumption = 0.0;
        }
    for (auto& gas : ccGas)
        {
        gas.consumption = 0.0;
        }
    decoSegments.clear();
    ceilings.clear();
    ttsList.clear();
    decoSeriesIndex.clear();
    pN2Tissues.clear();
    pHeTissues.clear();
    pN2MaxTissue.clear();
    pHeMaxTissue.clear();
    pMaxTissue.clear();
    }

void DivePlan::copy(const DivePlan& right)
    {
    planSegments = right.planSegments;
    gasList = right.gasList;
    ccGas = right.ccGas;
    }

void DivePlan::save(QDomNode& root, QDomDocument& doc) const
    {
    QDomElement gasListElement = doc.createElement("OCGasList");
    for (const auto& gas: gasList)
        {
        gas.save(gasListElement, doc);
        }
    root.appendChild(gasListElement);
    
    gasListElement = doc.createElement("CCGasList");
    for (const auto& gas: ccGas)
        {
        gas.save(gasListElement, doc);
        }
    root.appendChild(gasListElement);
    settings.save(root, doc);
    }
    
void DivePlan::read(QDomNode& root)
    {
    QDomElement gasListElement = root.firstChildElement("OCGasList");
    if ( ! gasListElement.isNull())
        {
        QDomElement gasElement = gasListElement.firstChildElement("Gas");
        int gasIndex = 0;
        while (!gasElement.isNull() && gasIndex < gasList.size())
            {
            gasList[gasIndex].read(gasElement);
            gasIndex++;
            gasElement = gasElement.nextSiblingElement("Gas");
            }
        }
        
    gasListElement = root.firstChildElement("CCGasList");
    if ( ! gasListElement.isNull())
        {
        QDomElement gasElement = gasListElement.firstChildElement("Gas");
        int gasIndex = 0;
        while (!gasElement.isNull() && gasIndex < ccGas.size())
            {
            ccGas[gasIndex].read(gasElement);
            gasIndex++;
            gasElement = gasElement.nextSiblingElement("Gas");
            }
        }
    settings.read(root);
    decoTypeChanged(settings.decoType);
    }


DiveSegment makeSegment(const QVector<DiveSample>& samples)
    {
    qreal timeSum = 0.0;
    qreal depthSum = 0.0;
    qreal depthAverage = 0.0;
    for (int i = 0; i < samples.size() - 1; ++i)
        {
        qreal timeDiff = samples[i+1].time - samples[i].time;
        qreal depthDiff = samples[i+1].depth - samples[i].depth;
        timeSum += timeDiff;
        depthSum += depthDiff;
        depthAverage += (samples[i+1].depth + samples[i].depth) * timeDiff / 2.0;
        }
    qreal slope = depthSum / timeSum;
    depthAverage /= timeSum;
    DiveSegment segment;
    segment.startTime = samples[0].time;
    segment.duration = samples[samples.size()-1].time + samples[0].time;
    segment.startDepth = depthAverage - slope * timeSum / 2.0;
    segment.endDepth = depthAverage + slope * timeSum / 2.0;
    return segment;
    }
    
qreal getMaxDeviation(const DiveSegment& segment, 
                      const QVector<DiveSample>& samples,
                      int startIndex, int endIndex)
    {
    qreal maxDeviation = 0.0;
    for (int i = startIndex; i < endIndex; ++i)
        {
        qreal depth = segment.getDepth(samples[i].time);
        qreal deviation = std::fabs(depth - samples[i].depth);
        maxDeviation = std::max(maxDeviation, deviation);
        }
    return maxDeviation;
    }

qreal toSeconds(const QString& timeStr)
    {
    QStringList parts = timeStr.section(' ', 0, 0).split(':');
    qreal factor = 1.0;
    qreal sec = 0.0;
    for (auto r = parts.rbegin(); r != parts.rend(); ++r)
        {
        sec += r->toInt() * factor;
        factor *= 60.0;
        }
    return sec;
    }

void DivePlan::readSSRF(QDomNode& root)
    {
    QDomElement divesElement = root.firstChildElement("dives");
    QDomElement diveElement = divesElement.firstChildElement("dive");
    QDomElement diveComputerElement = diveElement.firstChildElement("divecomputer");
    if ( not diveComputerElement.isNull())
        {
        QDomElement sampleElement = diveComputerElement.firstChildElement("sample");
        bool depthOkay;
        while ( ! sampleElement.isNull())
            {
            QString depthStr = sampleElement.attribute("depth");
            qreal depth = depthStr.section(' ', 0, 0).toDouble(&depthOkay);
            qreal time = toSeconds(sampleElement.attribute("time"));
            samples.append(DiveSample(time, depth));
            sampleElement = sampleElement.nextSiblingElement("sample");
            }
        }
    createPlanFromSamples();
    }
            
void DivePlan::createPlanFromSamples(qreal runtime)
    {
    clearDeco();
    planSegments.clear();
    planSeriesIndex.clear();
    DiveSegment segment = DiveSegment(0.0, 0.0);
    DiveSegment lastSegment;
    segment.decoMode = settings.decoType;
    if (settings.decoType == DecoSettings::CLOSED_CIRCUIT)
        segment.setPoint = settings.setPoint;
    int count = 0;
    int segmentStart = 0;
    int segmentEnd = 0;
    for (const auto& sample : samples)
        {
        lastSegment = segment;
        segment.duration = sample.time - segment.startTime;
        segment.endDepth = sample.depth;
        if (count > 2)
            {
            qreal deviation = getMaxDeviation(segment, samples,
                                              segmentStart, segmentEnd);
            //                 qDebug() << time << depth << deviation;
            if (deviation > deviationLimit)
                {
                if (lastSegment.startDepth < lastSegment.endDepth)
                    lastSegment.type = DiveSegment::DESCENT;
                else if (lastSegment.startDepth > lastSegment.endDepth)
                    lastSegment.type = DiveSegment::ASCENT;
                else
                    lastSegment.type = DiveSegment::STOP;
                planSegments.append(lastSegment);
                segmentStart = segmentEnd;
                count = 0;
                //                     qDebug() << "new segment";
                segment.startTime = lastSegment.startTime + lastSegment.duration;
                segment.startDepth = lastSegment.endDepth;
                }
            }
        ++segmentEnd;
        ++count;
        }
    if (runtime > 0.0)
        {
        int maxIndex = 0;
        for (int i = 0; i < planSegments.size(); ++i)
            {
            DiveSegment& s = planSegments[i];
            if (s.startTime > runtime)
                {
                maxIndex = i;
                break;
                }
            }
        planSegments.resize(maxIndex);
        }

    DiveSegment endSegment = DiveSegment(planSegments.back().runtime(),
                                         planSegments.back().endDepth);
    planSegments.append(endSegment);
    }


void DivePlan::appendSegment()
    {
    auto depth = planSegments.last().endDepth;
    auto startTime = planSegments.last().runtime();
    int gasIndex = planSegments.last().gasIndex;
    planSegments.append(DiveSegment(startTime, depth));
    planSegments.last().decoMode = settings.decoType;
    planSegments.last().gasIndex = gasIndex;
    if (settings.decoType == DecoSettings::CLOSED_CIRCUIT)
        planSegments.last().setPoint = settings.setPoint;
    }
    
void DivePlan::removeLastSegment()
    {
    planSegments.pop_back();
    }
    
void DivePlan::removeEmptySegments()
    {
    for (int i = 0; i < planSegments.size() - 1; ++i)
        {
        if (planSegments[i].duration <= 0)
            planSegments.remove(i--);
        }
    }

void DivePlan::fixSegments()
    {
    if (planSegments.last().duration > 0.0)
        appendSegment();
    removeEmptySegments();
    for (int i = 1; i < planSegments.size(); ++i)
        {
        planSegments[i].startDepth = planSegments[i - 1].endDepth;
        }
    planSeriesIndex.clear();
    }
    
void DivePlan::setDepth(int n, qreal depth)
    {
    planSegments[n].endDepth = depth;
    if (n == 0)
        planSegments[n].startDepth = 0.0;
    int m;
    if ((n + 1) < planSegments.size())
        {
        planSegments[n+1].startDepth = depth;
        m = n + 1;
        }
    else
        m = n;
    int delta = round((planSegments[n].endDepth
              - planSegments[n].startDepth) * 10);
    if (delta < 0)
        planSegments[n].type = DiveSegment::ASCENT;
    else if (delta > 0)
        planSegments[n].type = DiveSegment::DESCENT;
    else
        planSegments[n].type = DiveSegment::STOP;
    
    for (int i = 0; i < m + 1; ++i)
        {
        qreal currentDepth = planSegments[i].startDepth;
        if (planSegments[i].endDepth > planSegments[i].startDepth)
            {
            qreal minDuration = 0.0;
            while (currentDepth < planSegments[i].endDepth)
                {
                currentDepth += 1.0;
                qreal descentRate = settings.getDescentRate(currentDepth);
                minDuration += (1.0 / descentRate);
                }
            if (planSegments[i].duration < minDuration)
                planSegments[i].duration = minDuration;
            }
        else
            {
            qreal minDuration = 0.0;
            while (currentDepth > planSegments[i].endDepth)
                {
                currentDepth -= 1.0;
                qreal ascentRate = settings.getAscentRate(currentDepth);
                minDuration += (1.0 / ascentRate);
                }
            if (planSegments[i].duration < minDuration)
                planSegments[i].duration = minDuration;
            }
        }
    for (auto& segment : planSegments)
        segment.duration = std::floor(segment.duration);
    for (int i = n + 1; i < planSegments.size(); ++i)
        planSegments[i].startTime = planSegments[i - 1].runtime();
    }
    
void DivePlan::setDuration(int n, qreal duration)
    {
    if (duration >= 0)
        {
        planSegments[n].duration = duration;
        for (int i = n + 1; i < planSegments.size(); ++i)
            planSegments[i].startTime = planSegments[i - 1].runtime();
        }
    }
    
void DivePlan::decoTypeChanged(DecoSettings::DecoMode mode)
    {
    if (mode == DecoSettings::OPEN_CIRCUIT)
        {
        for (auto& segment : planSegments)
            {
            segment.gasIndex = getStartGas().index;
            segment.decoMode = mode;
            }
        }
    else if (mode == DecoSettings::CLOSED_CIRCUIT)
        {
        for (auto& segment : planSegments)
            {
            segment.gasIndex = 0;
            segment.decoMode = mode;
            segment.setPoint = settings.setPoint;
            }
        }
    }
    
DiveSegment* DivePlan::addDeco(DiveSegment& segment, DiveSegment::Type type)
    {
    int delta = round((segment.endDepth - segment.startDepth) * 10);
    if (delta < 0)
        segment.type = DiveSegment::ASCENT;
    else if (delta > 0)
        segment.type = DiveSegment::DESCENT;
    else
        segment.type = type;
    decoSegments.append(segment);
    decoSeriesIndex.clear();
    return &decoSegments.back();
    }
    
void DivePlan::getUserDepthList(QVector<QPointF> list) const
    {
    list.clear();
    list.reserve(planSegments.size() + 1);
    list.append(QPointF(planSegments[0].startTime, planSegments[0].startDepth));
    for (auto& segment : planSegments)
        list.append(QPointF(segment.runtime(), segment.endDepth));
    }
    
void DivePlan::getDecoDepthList(QVector<QPointF> list) const
    {
    list.clear();
    list.reserve(decoSegments.size() + 1);
    list.append(QPointF(decoSegments[0].startTime, decoSegments[0].startDepth));
    for (auto& segment : decoSegments)
        list.append(QPointF(segment.runtime(), segment.endDepth));
    }
    
int DivePlan::getPlanSeriesSize() const
    {
    if (planSeriesIndex.size() == 0)
        {
        int size = 0;
        int gasIndex = -1;
        for (int i = 0; i < planSegments.size(); ++i)
            {
            auto& segment = planSegments[i];
            if (segment.gasIndex != gasIndex)
                {
                planSeriesIndex.append(i);
                size++;
                gasIndex = segment.gasIndex;
                }
            }
        planSeriesIndex.append(planSegments.size());
        return size;
        }
    else
        return planSeriesIndex.size()-1;
    }

int DivePlan::getPlanSize(int n) const
    {
    if (planSeriesIndex.size() == 0)
        getPlanSeriesSize();
    return (planSeriesIndex[n+1] - planSeriesIndex[n] + 1);
    }

QPointF DivePlan::getPlanData(int n, int i) const
    {
    if (planSeriesIndex.size() == 0)
        getPlanSeriesSize();
    if (i == 0)
        {
        auto& segment = planSegments[planSeriesIndex[n]];
        return QPointF(segment.startTime, segment.startDepth);
        }
    else
        {
        auto& segment = planSegments[planSeriesIndex[n] + i - 1];
        return QPointF(segment.runtime(), segment.endDepth);
        }
    }

const DiveSegment& DivePlan::getPlanSegment(int n, int i) const
    {
    if (planSeriesIndex.size() == 0)
        getPlanSeriesSize();
    if (i == 0)
        {
        return planSegments[planSeriesIndex[n]];
        }
    else
        {
        return planSegments[planSeriesIndex[n] + i - 1];
        }
    }

const Gas& DivePlan::getPlanGas(int n) const
    {
    if (planSeriesIndex.size() == 0)
        getPlanSeriesSize();
    auto& segment = planSegments[planSeriesIndex[n]];
    return getGas(segment.gasIndex, segment.decoMode);
    }
    
int DivePlan::getDecoSeriesSize() const
    {
    if (decoSeriesIndex.size() == 0)
        {
        int size = 0;
        int gasIndex = -1;
        DecoSettings::DecoMode mode = DecoSettings::OPEN_CIRCUIT;
        for (int i = 0; i < decoSegments.size(); ++i)
            {
            auto& segment = decoSegments[i];
            if (segment.gasIndex != gasIndex || segment.decoMode != mode)
                {
                decoSeriesIndex.append(i);
                size++;
                gasIndex = segment.gasIndex;
                mode = segment.decoMode;
                }
            }
        decoSeriesIndex.append(decoSegments.size());
        return size;
        }
    else
        return decoSeriesIndex.size()-1;
    }

int DivePlan::getDecoSize(int n) const
    {
    if (decoSeriesIndex.size() == 0)
        getDecoSeriesSize();
    return (decoSeriesIndex[n+1] - decoSeriesIndex[n] + 1);
    }

QPointF DivePlan::getDecoData(int n, int i) const
    {
    if (decoSeriesIndex.size() == 0)
        getDecoSeriesSize();
    if (i == 0)
        {
        auto& segment = decoSegments[decoSeriesIndex[n]];
        return QPointF(segment.startTime, segment.startDepth);
        }
    else
        {
        auto& segment = decoSegments[decoSeriesIndex[n] + i - 1];
        return QPointF(segment.runtime(), segment.endDepth);
        }
    }

const DiveSegment& DivePlan::getDecoSegment(int n, int i) const
    {
    if (decoSeriesIndex.size() == 0)
        getDecoSeriesSize();
    if (i == 0)
        {
        return decoSegments[decoSeriesIndex[n]];
        }
    else
        {
        return decoSegments[decoSeriesIndex[n] + i - 1];
        }
    }

const Gas& DivePlan::getDecoGas(int n) const
    {
    if (decoSeriesIndex.size() == 0)
        getDecoSeriesSize();
    auto& segment = decoSegments[decoSeriesIndex[n]];
    return getGas(segment.gasIndex, segment.decoMode);
    }
    
qreal DivePlan::getMaxDepth() const
    {
    qreal maxDepth = 0.0;
    for (auto& segment : planSegments)
        {
        maxDepth = qMax(maxDepth, segment.endDepth);
        }
    return maxDepth;
    }
   
qreal DivePlan::getMaxPressure() const
    {
    return (getMaxDepth() * DecoSettings::MeterToPa + settings.surfacePressure);
    }
   
qreal DivePlan::getDepthForPressure(qreal pressure) const
    {
    return ((pressure - settings.surfacePressure) / DecoSettings::MeterToPa);
    }
    
boost::tuple<qreal, DiveSegment*> DivePlan::getDepth(qreal time)
    {
    boost::tuple<qreal, DiveSegment*> result;
    for (int i = 0; i < planSegments.size() - 1; ++i)
        {
        auto delta = (planSegments[i].runtime() - time);
        if (delta > 0.0 && delta <= planSegments[i].duration)
            {
            auto m = (delta / planSegments[i].duration);
            qreal depth = planSegments[i].startDepth * m 
                        + planSegments[i].endDepth * (1.0 - m);
            result.get<0>() = depth;
            result.get<1>() = &planSegments[i];
            return result;
            }
        }
    result.get<0>() = planSegments.last().endDepth;
    result.get<1>() = &planSegments.last();
    return result;
    }
    
qreal DivePlan::getUserRuntime() const
    {
    return std::floor(planSegments.last().runtime());
    }

qreal DivePlan::getDecoRuntime() const
    {
    if ( ! decoSegments.empty() )
        return std::floor(decoSegments.last().runtime());
    else
        return 0.0;
    }
    
boost::tuple<qreal, DiveSegment*> DivePlan::getReverseDepth(qreal runtime)
    {
    qreal returnTime = getUserRuntime();
    qreal time = returnTime - (runtime  * settings.returnFactor);
    if (time > 0.0)
        return getDepth(time);
    else
        return boost::tuple<qreal, DiveSegment*>(0, nullptr);
    }
    
int DivePlan::getGasSize() const
    {
    if (settings.decoType == DecoSettings::OPEN_CIRCUIT)
        return gasList.size();
    else
        return ccGas.size();
    }

Gas& DivePlan::getGas(int n)
    {
    if (settings.decoType == DecoSettings::OPEN_CIRCUIT)
        return gasList[n];
    else
        return ccGas[n];
    }
    
const Gas& DivePlan::getGas(int n) const
    {
    if (settings.decoType == DecoSettings::OPEN_CIRCUIT)
        return gasList[n];
    else
        return ccGas[n];
    }
    
Gas& DivePlan::getGas(int n, DecoSettings::DecoMode decoType)
    {
    if (decoType == DecoSettings::OPEN_CIRCUIT)
        return gasList[n];
    else
        return ccGas[n];
    }
    
const Gas& DivePlan::getGas(int n, DecoSettings::DecoMode decoType) const
    {
    if (decoType == DecoSettings::OPEN_CIRCUIT)
        return gasList[n];
    else
        return ccGas[n];
    }
    
const Gas& DivePlan::getStartGas() const
    {
    int gasIndex = 0;
    for (int i = 0; i < gasList.size(); ++i)
        {
        if (gasList[i].active && gasList[i].changeDepth == 0.0)
            gasIndex = i;
        }
     
    return gasList[gasIndex];
    }
      
Gas& DivePlan::getBestGas(qreal depth)
    {
    int gasIndex = -1;
    for (int i = 0; i < gasList.size(); ++i)
        {
        if (gasList[i].active && 
            ( gasList[i].changeDepth == 0.0 || depth <= gasList[i].changeDepth))
            if (gasIndex < 0 || gasList[i].oxygen > gasList[gasIndex].oxygen)
                gasIndex = i;
        }
    return gasList[gasIndex];
    }

Gas& DivePlan::getCurrentGas(qreal time)
    {
    for (const DiveSegment& segment : planSegments)
        {
        qreal delta = segment.runtime() - time;
        if (delta > 0.0 && delta <= segment.duration)
            {
            return getGas(segment.gasIndex, segment.decoMode);
            }
        }
    return gasList[planSegments.back().gasIndex];
    }
    
void DivePlan::getOxygenList(QVector<QPointF>& list) const
    {
    list.clear();
    qreal pO2;
    for (const DiveSegment& segment : decoSegments)
        {
        qreal pressure = settings.getPressure(segment.startDepth);
        if (segment.decoMode == DecoSettings::CLOSED_CIRCUIT)
            pO2 = std::min(segment.setPoint * settings.BarToPa, pressure);
        else
            pO2 =  getGas(segment.gasIndex, segment.decoMode).oxygen * pressure;
        list.append(QPointF(segment.startTime, pO2 * settings.PaToBar));
        
        pressure = settings.getPressure(segment.endDepth);
        if (segment.decoMode == DecoSettings::CLOSED_CIRCUIT)
            pO2 = std::min(segment.setPoint * settings.BarToPa, pressure);
        else
            pO2 =  getGas(segment.gasIndex, segment.decoMode).oxygen * pressure;
        list.append(QPointF(segment.runtime(), pO2 * settings.PaToBar));
        }
    }
    
void DivePlan::getNitrogenList(QVector<QPointF>& list) const
    {
    list.clear();
    qreal pN2;
    for (const DiveSegment& segment : decoSegments)
        {
        qreal pressure = settings.getPressure(segment.startDepth);
        if (segment.decoMode == DecoSettings::CLOSED_CIRCUIT)
            {
            qreal pDiluent = std::max(pressure 
                                      - segment.setPoint * settings.BarToPa
                                      , 0.0);
            qreal fN2 = getGas(segment.gasIndex, segment.decoMode).nitrogen 
                       / (getGas(segment.gasIndex, segment.decoMode).nitrogen 
                          + getGas(segment.gasIndex, segment.decoMode).helium);
            pN2 =  pDiluent * fN2;
            }
        else
            pN2 =  pressure * getGas(segment.gasIndex, segment.decoMode).nitrogen;
        list.append(QPointF(segment.startTime, pN2 * settings.PaToBar));
        
        pressure = settings.getPressure(segment.endDepth);
        if (segment.decoMode == DecoSettings::CLOSED_CIRCUIT)
            {
            qreal pDiluent = std::max(pressure 
                                      - segment.setPoint * settings.BarToPa
                                      , 0.0);
            qreal fN2 = getGas(segment.gasIndex, segment.decoMode).nitrogen 
                       / (getGas(segment.gasIndex, segment.decoMode).nitrogen 
                          + getGas(segment.gasIndex, segment.decoMode).helium);
            pN2 =  pDiluent * fN2;
            }
        else
            pN2 =  pressure * getGas(segment.gasIndex, segment.decoMode).nitrogen;
        list.append(QPointF(segment.runtime(), pN2 * settings.PaToBar));
        }
    }
    
void DivePlan::getHeliumList(QVector<QPointF>& list) const
    {
    list.clear();
    qreal pHe;
    for (const DiveSegment& segment : decoSegments)
        {
        qreal pressure = settings.getPressure(segment.startDepth);
        if (segment.decoMode == DecoSettings::CLOSED_CIRCUIT)
            {
            qreal pDiluent = std::max(pressure 
                                      - segment.setPoint * settings.BarToPa
                                      , 0.0);
            qreal fHe = getGas(segment.gasIndex, segment.decoMode).helium 
                       / (getGas(segment.gasIndex, segment.decoMode).nitrogen 
                          + getGas(segment.gasIndex, segment.decoMode).helium);
            pHe =  pDiluent * fHe;
            }
        else
            pHe =  pressure * getGas(segment.gasIndex, segment.decoMode).helium;
        list.append(QPointF(segment.startTime, pHe * settings.PaToBar));
        
        pressure = settings.getPressure(segment.endDepth);
        if (segment.decoMode == DecoSettings::CLOSED_CIRCUIT)
            {
            qreal pDiluent = std::max(pressure 
                                      - segment.setPoint * settings.BarToPa
                                      , 0.0);
            qreal fHe = getGas(segment.gasIndex, segment.decoMode).helium 
                       / (getGas(segment.gasIndex, segment.decoMode).nitrogen 
                          + getGas(segment.gasIndex, segment.decoMode).helium);
            pHe =  pDiluent * fHe;
            }
        else
            pHe =  pressure * getGas(segment.gasIndex, segment.decoMode).helium;
        list.append(QPointF(segment.runtime(), pHe * settings.PaToBar));
        }
    }
    
void DivePlan::getNarcosisList(QVector<QPointF>& list) const
    {
    list.clear();
    qreal pO2;
    qreal pN2;
    for (const DiveSegment& segment : decoSegments)
        {
        qreal pressure = settings.getPressure(segment.startDepth);
        if (segment.decoMode == DecoSettings::CLOSED_CIRCUIT)
            {
            pO2 = std::min(segment.setPoint * settings.BarToPa, pressure);
            qreal pDiluent = std::max(pressure 
                                      - segment.setPoint * settings.BarToPa
                                      , 0.0);
            qreal fN2 = getGas(segment.gasIndex, segment.decoMode).nitrogen 
                       / (getGas(segment.gasIndex, segment.decoMode).nitrogen 
                          + getGas(segment.gasIndex, segment.decoMode).helium);
            pN2 =  pDiluent * fN2;
            }
        else
            {
            pN2 =  pressure * getGas(segment.gasIndex, segment.decoMode).nitrogen;
            pO2 =  getGas(segment.gasIndex, segment.decoMode).oxygen * pressure;
            }
        list.append(QPointF(segment.startTime, 
                            (pO2 + pN2) * settings.PaToBar));
        
        pressure = settings.getPressure(segment.endDepth);
        if (segment.decoMode == DecoSettings::CLOSED_CIRCUIT)
            {
            pO2 = std::min(segment.setPoint * settings.BarToPa, pressure);
            qreal pDiluent = std::max(pressure 
                                      - segment.setPoint * settings.BarToPa
                                      , 0.0);
            qreal fN2 = getGas(segment.gasIndex, segment.decoMode).nitrogen 
                       / (getGas(segment.gasIndex, segment.decoMode).nitrogen 
                          + getGas(segment.gasIndex, segment.decoMode).helium);
            pN2 =  pDiluent * fN2;
            }
        else
            {
            pN2 =  pressure * getGas(segment.gasIndex, segment.decoMode).nitrogen;
            pO2 =  getGas(segment.gasIndex, segment.decoMode).oxygen * pressure;
            }
        list.append(QPointF(segment.runtime(), 
                            (pO2 + pN2) * settings.PaToBar));
        }
    }

void DivePlan::createDivePlanDoc(QTextCursor& cursor) const
    {
    qreal bottomTime = getUserRuntime();
    qreal runTime = getDecoRuntime();
    qreal tts = runTime - bottomTime;
    cursor.insertText(QObject::tr("Bottom time:\t%1 min\n").arg(timeLabel(bottomTime)));
    cursor.insertText(QObject::tr("Time to surface:\t%1 min\n").arg(timeLabel(tts)));
    cursor.insertText(QObject::tr("Run time:\t%1 min\n").arg(timeLabel(runTime)));
    qreal end = 0.0;
    for (const DiveSegment& segment : decoSegments)
        {
        qreal pN2;
        qreal pressure = settings.getPressure(std::max(segment.startDepth,segment.endDepth));
        if (segment.decoMode == DecoSettings::CLOSED_CIRCUIT)
            {
            qreal pDiluent = std::max(pressure 
                                      - segment.setPoint * settings.BarToPa
                                      , 0.0);
            qreal fN2 = getGas(segment.gasIndex, segment.decoMode).nitrogen 
                       / (getGas(segment.gasIndex, segment.decoMode).nitrogen 
                          + getGas(segment.gasIndex, segment.decoMode).helium);
            pN2 =  pDiluent * fN2;
            }
        else
            {
            pN2 =  pressure * getGas(segment.gasIndex, segment.decoMode).nitrogen;
            }
        pressure = pN2 * 1.26582278481;
        end = std::max(end, getDepthForPressure(pressure));
        }
    cursor.insertText(QObject::tr("END:\t%1 m\n").arg(end, 3,'f', 0));
    for (const auto& gas: gasList)
        {
        if (gas.consumption > 0.0)
            {
            cursor.insertText(QObject::tr("Gas %1 %2:\t %3 l\n").arg(gas.index + 1)
                               .arg(gas.str()).arg(gas.consumption, 0,'f', 0));
            }
        }
    }
    
QString timeLabel(qreal time)
    {
    return QString("%1:%2").arg(int(time/60.0))
                           .arg(int(fmod(time, 60.0)), 2, 10, QChar('0'));
    }
        
        
