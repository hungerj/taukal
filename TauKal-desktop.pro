CONFIG += release
QT += xml
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport concurrent
TEMPLATE = app
TARGET = taukal
OBJECTS_DIR = obj
MOC_DIR = moc
QMAKE_CXXFLAGS += -std=c++14
DEFINES += UNIX
DEFINES += TEMPBUILD
unix:INCLUDEPATH += /usr/include/qwt
LIBS += -lqwt-qt5
DEPENDPATH += . \
              code_part1/OSTC_code_asm_part1 \
              code_part1/OSTC_code_c_part2

INCLUDEPATH += . \
               code_part1/OSTC_code_c_part2 \
               code_part1/OSTC_code_asm_part1

RESOURCES = taukal.qrc

TRANSLATIONS = taukal_de.ts

# Input
HEADERS += \
           diveData.h \
           decoCalc.h \
           decoOSTC.h \
           code_part1/OSTC_code_asm_part1/shared_definitions.h \
           code_part1/OSTC_code_c_part2/p2_definitions.h \
           diveModel.h \
           settingsEditor.h \
           gasEditor.h \
           divePlot.h \
           phoneMenu.h \
           diveEditor.h
           
           
SOURCES += \
           diveData.cpp \
           decoCalc.cpp \
           decoOSTC.cpp \
           code_part1/OSTC_code_c_part2/p2_deco.c \
           diveModel.cpp \
           settingsEditor.cpp \
           gasEditor.cpp \
           divePlot.cpp \
           phoneMenu.cpp \
           diveEditor.cpp \
           main.cpp
