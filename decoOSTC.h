#ifndef DECO_OSTC_H
#define DECO_OSTC_H

#include <QtGlobal>
#include <QVector>

struct DecoStop
    {
    qreal duration;
    qreal depth;
    };

struct Compartiment
    {
    qreal n2;
    qreal he;
    };

class DecoOSTC
    {
    public:
    
    DecoOSTC();
    
    typedef enum {ZH_L16_OC, Gauge, ZH_L16_CC, Apnoe, ZH_L16_GF_OC, ZH_L16_GF_CC} DecoType;

    ~DecoOSTC();
    
    void reset();
    void setSurfacePressure(qreal p_bar);
    void setDepth(qreal depth);
    
    void nextDiveStep(qreal time);
    
    qreal getNonDecoLimit();
    qreal getAscentTime();
    
    int getDecoStops();
    DecoStop getDecoStop(int n);
    Compartiment getCompartiment(int n);
    qreal pTissueN2(int n) const;
    qreal pTissueHe(int n) const;
    
    void setGradientFactors(qreal low, qreal high);
    void setDecoType(DecoType type);
    void setGas(int n, qreal changeDepth, qreal n2_ratio, qreal he_ratio);
    void setSetPoint(qreal setPoint);
    void setDecoDistance(qreal dist);
    void setLastDecoDepth(qreal depth);
    void setCurrentGas(int n);
    private:
    qreal diveTime;
    QVector<DecoStop> decoStops;
    };
    
#endif
