<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="de_DE">
<context>
    <name>AscentRateModel</name>
    <message>
        <location filename="diveModel.cpp" line="271"/>
        <source>Depth [m]</source>
        <translation>Tiefe [m]</translation>
    </message>
    <message>
        <location filename="diveModel.cpp" line="272"/>
        <source>Rate [m/min]</source>
        <translation>Geschw. [m/min]</translation>
    </message>
</context>
<context>
    <name>DesktopDiveEditor</name>
    <message>
        <location filename="diveEditor.cpp" line="258"/>
        <source>Plan</source>
        <translation>Plan</translation>
    </message>
    <message>
        <location filename="diveEditor.cpp" line="259"/>
        <source>Deco</source>
        <translation>Deko</translation>
    </message>
    <message>
        <location filename="diveEditor.cpp" line="260"/>
        <source>Bailout</source>
        <translation>Notfall</translation>
    </message>
    <message>
        <location filename="diveEditor.cpp" line="261"/>
        <source>Settings</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <location filename="diveEditor.cpp" line="262"/>
        <source>Gases</source>
        <translation>Gase</translation>
    </message>
    <message>
        <location filename="diveEditor.cpp" line="277"/>
        <source>Profile</source>
        <translation>Profil</translation>
    </message>
    <message>
        <location filename="diveEditor.cpp" line="284"/>
        <source>Pressure</source>
        <translation>Druck</translation>
    </message>
    <message>
        <location filename="diveEditor.cpp" line="291"/>
        <source>Tissues</source>
        <translation>Gewebe</translation>
    </message>
</context>
<context>
    <name>DiveEditor</name>
    <message>
        <location filename="diveEditor.cpp" line="117"/>
        <source>Clear</source>
        <translation>Neu</translation>
    </message>
    <message>
        <location filename="diveEditor.cpp" line="120"/>
        <source>Clear Deco</source>
        <translation>Deko löschen</translation>
    </message>
    <message>
        <location filename="diveEditor.cpp" line="123"/>
        <source>Calculate</source>
        <translation>Berechnen</translation>
    </message>
    <message>
        <location filename="diveEditor.cpp" line="129"/>
        <source>Gas Pressures</source>
        <translation>Partialdrücke</translation>
    </message>
    <message>
        <location filename="diveEditor.cpp" line="131"/>
        <source>Bailout Pressures</source>
        <translation>Partialdrücke Notfall</translation>
    </message>
    <message>
        <location filename="diveEditor.cpp" line="134"/>
        <source>Nitrogen Saturation</source>
        <translation>Stickstoff Sättigung</translation>
    </message>
    <message>
        <location filename="diveEditor.cpp" line="136"/>
        <source>Helium Saturation</source>
        <translation>Helium Sättigung</translation>
    </message>
    <message>
        <location filename="diveEditor.cpp" line="232"/>
        <source>DOM Bookmarks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="diveEditor.cpp" line="233"/>
        <source>Parse error at line %1, column %2:
%3</source>
        <translation>Einlesefehler in Zeile %1, Spalte %2: %3</translation>
    </message>
</context>
<context>
    <name>DivePlanPlot</name>
    <message>
        <location filename="divePlot.cpp" line="128"/>
        <source>[min:s]</source>
        <translation>[min:s]</translation>
    </message>
    <message>
        <location filename="divePlot.cpp" line="130"/>
        <source>[m]</source>
        <translation>[m]</translation>
    </message>
    <message>
        <location filename="divePlot.cpp" line="135"/>
        <source>Ceiling</source>
        <translation>Deko</translation>
    </message>
    <message>
        <location filename="divePlot.cpp" line="151"/>
        <source>Max Tissue P</source>
        <translation>Max Gewebe Druck</translation>
    </message>
</context>
<context>
    <name>DiveSegmentModel</name>
    <message>
        <location filename="diveModel.cpp" line="136"/>
        <source>Type</source>
        <translation>Typ</translation>
    </message>
    <message>
        <location filename="diveModel.cpp" line="137"/>
        <source>Runtime</source>
        <translation>Laufzeit</translation>
    </message>
    <message>
        <location filename="diveModel.cpp" line="138"/>
        <source>Duration</source>
        <translation>Dauer</translation>
    </message>
    <message>
        <location filename="diveModel.cpp" line="139"/>
        <source>End Depth</source>
        <translation>Endtiefe</translation>
    </message>
    <message>
        <location filename="diveModel.cpp" line="140"/>
        <source>Gas</source>
        <translation>Gas</translation>
    </message>
    <message>
        <location filename="diveModel.cpp" line="142"/>
        <source>Consumption</source>
        <translation>Verbrauch</translation>
    </message>
    <message>
        <location filename="diveModel.cpp" line="143"/>
        <source>Descent</source>
        <translation>Abstieg</translation>
    </message>
    <message>
        <location filename="diveModel.cpp" line="144"/>
        <source>Ascent</source>
        <translation>Aufstieg</translation>
    </message>
    <message>
        <location filename="diveModel.cpp" line="145"/>
        <source>Stop</source>
        <translation>Stop</translation>
    </message>
    <message>
        <location filename="diveModel.cpp" line="146"/>
        <source>Problem</source>
        <translation>Problem</translation>
    </message>
    <message>
        <location filename="diveModel.cpp" line="147"/>
        <source>Gas Change</source>
        <translation>Gaswechsel</translation>
    </message>
</context>
<context>
    <name>GasEditor</name>
    <message>
        <location filename="gasEditor.cpp" line="211"/>
        <source>Max Po2 Bottom [bar]</source>
        <translation>Max PO2 Grund</translation>
    </message>
    <message>
        <location filename="gasEditor.cpp" line="217"/>
        <source>Max Po2 Deco [bar]</source>
        <translation>Max PO2 Deko</translation>
    </message>
    <message>
        <location filename="gasEditor.cpp" line="224"/>
        <source>Open Circuit Gas List</source>
        <translation>Gase OC</translation>
    </message>
    <message>
        <location filename="gasEditor.cpp" line="236"/>
        <source>Closed Circuit Gas List</source>
        <translation>Gase CC</translation>
    </message>
</context>
<context>
    <name>GasListModel</name>
    <message>
        <location filename="gasEditor.cpp" line="80"/>
        <source>Active</source>
        <translation>Aktiv</translation>
    </message>
    <message>
        <location filename="gasEditor.cpp" line="81"/>
        <source>O2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="gasEditor.cpp" line="82"/>
        <source>He</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="gasEditor.cpp" line="83"/>
        <source>N2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="gasEditor.cpp" line="84"/>
        <source>MOD</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="gasEditor.cpp" line="85"/>
        <source>Depth</source>
        <translation>Tiefe</translation>
    </message>
    <message>
        <location filename="gasEditor.cpp" line="86"/>
        <source>RMV</source>
        <translation>AMV</translation>
    </message>
</context>
<context>
    <name>PhoneDiveEditor</name>
    <message>
        <location filename="diveEditor.cpp" line="392"/>
        <source>Plan</source>
        <translation>Plan</translation>
    </message>
    <message>
        <location filename="diveEditor.cpp" line="393"/>
        <source>Profile</source>
        <translation>Profil</translation>
    </message>
    <message>
        <location filename="diveEditor.cpp" line="394"/>
        <source>Deco</source>
        <translation>Deko</translation>
    </message>
    <message>
        <location filename="diveEditor.cpp" line="395"/>
        <source>Bailout</source>
        <translation>Notfall</translation>
    </message>
    <message>
        <location filename="diveEditor.cpp" line="396"/>
        <source>Pressure</source>
        <translation>Druck</translation>
    </message>
    <message>
        <location filename="diveEditor.cpp" line="397"/>
        <source>Settings</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <location filename="diveEditor.cpp" line="398"/>
        <source>Gases</source>
        <translation>Gase</translation>
    </message>
</context>
<context>
    <name>PressurePlot</name>
    <message>
        <location filename="divePlot.cpp" line="290"/>
        <source>[min:s]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="divePlot.cpp" line="291"/>
        <source>[bar]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="divePlot.cpp" line="298"/>
        <source>O2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="divePlot.cpp" line="303"/>
        <source>N2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="divePlot.cpp" line="307"/>
        <source>He</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="divePlot.cpp" line="311"/>
        <source>N2 + O2</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SettingsEditor</name>
    <message>
        <location filename="settingsEditor.cpp" line="172"/>
        <source>Open Circuit</source>
        <translation>Offen (OC)</translation>
    </message>
    <message>
        <location filename="settingsEditor.cpp" line="175"/>
        <source>Closed Circuit</source>
        <translation>Kreislaufgerät (CC)</translation>
    </message>
    <message>
        <location filename="settingsEditor.cpp" line="226"/>
        <source>Round times</source>
        <translation>Zeiten runden</translation>
    </message>
    <message>
        <location filename="settingsEditor.cpp" line="227"/>
        <source>GF Low</source>
        <translation>Unterer GF</translation>
    </message>
    <message>
        <location filename="settingsEditor.cpp" line="228"/>
        <source>GF High</source>
        <translation>Oberer GF</translation>
    </message>
    <message>
        <location filename="settingsEditor.cpp" line="229"/>
        <source>Bailout GF Low</source>
        <translation>Unterer GF Notfall</translation>
    </message>
    <message>
        <location filename="settingsEditor.cpp" line="230"/>
        <source>Bailout GF High</source>
        <translation>Oberer GF Notfall</translation>
    </message>
    <message>
        <location filename="settingsEditor.cpp" line="231"/>
        <source>Surface Pressure [mbar]</source>
        <translation>Oberflächendruck [mbar]</translation>
    </message>
    <message>
        <location filename="settingsEditor.cpp" line="232"/>
        <source>Density (Salinity) [kg/l]</source>
        <translation>Dichte (Salzgehalt) [kg/l]</translation>
    </message>
    <message>
        <location filename="settingsEditor.cpp" line="235"/>
        <source>CCR O2 Set Point [bar]</source>
        <translation>CCR PO2 Vorgabe [bar]</translation>
    </message>
    <message>
        <location filename="settingsEditor.cpp" line="236"/>
        <source>Last Deco Stop Depth [m]</source>
        <translation>Tiefe letzter Deko Stop [m]</translation>
    </message>
    <message>
        <location filename="settingsEditor.cpp" line="237"/>
        <source>Distance to Deco Stop [m]</source>
        <translation>Abstand zum Dekostop [m]</translation>
    </message>
    <message>
        <location filename="settingsEditor.cpp" line="238"/>
        <source>Stop time for gas change [s]</source>
        <translation>Gaswechseldauer [s]</translation>
    </message>
    <message>
        <location filename="settingsEditor.cpp" line="239"/>
        <source>Problem solving time [s]</source>
        <translation>Problemdauer [s]</translation>
    </message>
    <message>
        <location filename="settingsEditor.cpp" line="240"/>
        <source>Bailout RMV Factor</source>
        <translation>Notfall AMV Faktor</translation>
    </message>
    <message>
        <location filename="settingsEditor.cpp" line="242"/>
        <source>Descent Rate</source>
        <translation>Abstiegsgeschw.</translation>
    </message>
    <message>
        <location filename="settingsEditor.cpp" line="244"/>
        <source>Ascent Rate</source>
        <translation>Aufstiegsgeschw.</translation>
    </message>
</context>
<context>
    <name>TabletDiveEditor</name>
    <message>
        <location filename="diveEditor.cpp" line="314"/>
        <source>Plan</source>
        <translation>Plan</translation>
    </message>
    <message>
        <location filename="diveEditor.cpp" line="315"/>
        <source>Deco</source>
        <translation>Deko</translation>
    </message>
    <message>
        <location filename="diveEditor.cpp" line="316"/>
        <source>Bailout</source>
        <translation>Notfall</translation>
    </message>
    <message>
        <location filename="diveEditor.cpp" line="326"/>
        <source>Pressure</source>
        <translation>Druck</translation>
    </message>
    <message>
        <location filename="diveEditor.cpp" line="327"/>
        <source>Settings</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <location filename="diveEditor.cpp" line="328"/>
        <source>Gases</source>
        <translation>Gase</translation>
    </message>
</context>
<context>
    <name>TissuePlot</name>
    <message>
        <location filename="divePlot.cpp" line="421"/>
        <source>[min:s]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="divePlot.cpp" line="422"/>
        <source>[bar]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="divePlot.cpp" line="426"/>
        <source>N2</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
