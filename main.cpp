#include "diveEditor.h"

#include <algorithm>

#include <QApplication>
#include <QLocale>
#include <QTextCodec>
#include <QTranslator>

#ifdef TAUKALMOBILE

#include <QGuiApplication>
#include <QScreen>
#include <QDebug>
#include <QDir>

QString Orientation(Qt::ScreenOrientation orientation)
{
    switch (orientation) {
        case Qt::PrimaryOrientation           : return "Primary";
        case Qt::LandscapeOrientation         : return "Landscape";
        case Qt::PortraitOrientation          : return "Portrait";
        case Qt::InvertedLandscapeOrientation : return "Inverted landscape";
        case Qt::InvertedPortraitOrientation  : return "Inverted portrait";
        default                               : return "Unknown";
    }
}
#endif

int main(int argc, char** argv)
    {
    QApplication app(argc, argv);
    QString locale = QLocale::system().name();

    QTranslator translator;
    translator.load(QString("taukal_") + locale);
    app.installTranslator(&translator);
    
    DiveEditor* editor;
#ifdef TAUKALMOBILE
      foreach (QScreen *screen, QGuiApplication::screens()) 
        {
        qDebug() << "Information for screen:" << screen->name();
        qDebug() << "  Available geometry:" << screen->availableGeometry().x() << screen->availableGeometry().y() << screen->availableGeometry().width() << "x" << screen->availableGeometry().height();
        qDebug() << "  Available size:" << screen->availableSize().width() << "x" << screen->availableSize().height();
        qDebug() << "  Available virtual geometry:" << screen->availableVirtualGeometry().x() << screen->availableVirtualGeometry().y() << screen->availableVirtualGeometry().width() << "x" << screen->availableVirtualGeometry().height();
        qDebug() << "  Available virtual size:" << screen->availableVirtualSize().width() << "x" << screen->availableVirtualSize().height();
        qDebug() << "  Depth:" << screen->depth() << "bits";
        qDebug() << "  Geometry:" << screen->geometry().x() << screen->geometry().y() << screen->geometry().width() << "x" << screen->geometry().height();
        qDebug() << "  Logical DPI:" << screen->logicalDotsPerInch();
        qDebug() << "  Logical DPI X:" << screen->logicalDotsPerInchX();
        qDebug() << "  Logical DPI Y:" << screen->logicalDotsPerInchY();
        qDebug() << "  Orientation:" << Orientation(screen->orientation());
        qDebug() << "  Physical DPI:" << screen->physicalDotsPerInch();
        qDebug() << "  Physical DPI X:" << screen->physicalDotsPerInchX();
        qDebug() << "  Physical DPI Y:" << screen->physicalDotsPerInchY();
        qDebug() << "  Physical size:" << screen->physicalSize().width() << "x" << screen->physicalSize().height() << "mm";
        qDebug() << "  Primary orientation:" << Orientation(screen->primaryOrientation());
        qDebug() << "  Refresh rate:" << screen->refreshRate() << "Hz";
        qDebug() << "  Size:" << screen->size().width() << "x" << screen->size().height();
        qDebug() << "  Virtual geometry:" << screen->virtualGeometry().x() << screen->virtualGeometry().y() << screen->virtualGeometry().width() << "x" << screen->virtualGeometry().height();
        qDebug() << "  Virtual size:" << screen->virtualSize().width() << "x" << screen->virtualSize().height();
        }
    qDebug() << "Home: " <<  QDir::homePath();
    qDebug() << "cwd: " <<  QDir::currentPath();
    

    QScreen* screen = QGuiApplication::screens()[0];
    int longSide = std::max(screen->physicalSize().width(), screen->physicalSize().height());
    if (longSide < 150)
        editor = new PhoneDiveEditor;
    else
        editor = new TabletDiveEditor;
    #else
//    QTextCodec::setCodecForTr(QTextCodec::codecForName("utf8"));
    editor = new DesktopDiveEditor;
    #endif
    
    editor->show();
    return app.exec();
    }
