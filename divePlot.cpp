//  TauKal: A dive decompression plan software
//  Copyright (C) 2012  Joerg Hunger
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "divePlot.h"

#include <qwt_plot_grid.h>
#include <qwt_scale_engine.h>
#include <qwt_legend.h>
#include <qwt_legend_label.h>
#include <qwt_plot_marker.h>
#include <qwt_plot_panner.h>
#include <qwt_plot_magnifier.h>
#include <qwt_plot_zoomer.h>
#include <qwt_plot_picker.h>
#include <qwt_picker_machine.h>

#include <QCheckBox>
#include <QHBoxLayout>
#include <QAction>

const QList<double> makeTicks(qreal tMin, qreal tMax)
    {
    static QVector<double> baseList = { 1.0, 2.0, 5.0, 10.0, 15.0, 30.0, 60.0, 
                                       120.0, 300.0, 600.0, 900,0, 1800.0, 3600.0};
    double interval = (tMax - tMin) / 10.0;
    double base = baseList.last();
    for (auto b : baseList)
        {
        if (b > interval)
            {
            base = b;
            break;
            }
        }
    double start = std::floor(tMin / base) * base;
    QList<double> ticks;
    for (int i = 0; i < 11; ++i)
        ticks.append(start + i * base);
    return ticks;
    }
    

PlanData::PlanData(DivePlan& _plan, int _n)
    : plan(_plan),
      n(_n)
    {
    }
    
QPointF PlanData::sample(size_t i) const
    {
    return plan.getPlanData(n, i);
    }
    
size_t PlanData::size() const
    {
    return plan.getPlanSize(n);
    }
    
QRectF PlanData::boundingRect() const
    {
    return QRectF(0.0, 0.0, plan.getUserRuntime(), plan.getMaxDepth());
    }

DiveSamplesData::DiveSamplesData(DivePlan& _plan)
    : plan(_plan)
    {
    }
    
QPointF DiveSamplesData::sample(size_t i) const
    {
    return plan.samples[i].getPoint();
    }
    
size_t DiveSamplesData::size() const
    {
    return plan.samples.size();
    }
    
QRectF DiveSamplesData::boundingRect() const
    {
    qreal maxTime = 1.0;
    if ( not plan.samples.empty())
    {
        maxTime = plan.samples[plan.samples.size() - 1].time;
    }
    qreal maxDepth = 0.0;
    for (const auto& sample : plan.samples)
        {
        maxDepth = std::max(maxDepth, sample.depth);
        }
    return QRectF(0.0, 0.0, maxTime, maxDepth);
    }

DecoData::DecoData(DivePlan& _plan, int _n)
    : plan(_plan),
      n(_n)
    {
    }
    
QPointF DecoData::sample(size_t i) const
    {
    return plan.getDecoData(n, i);
    }
    
size_t DecoData::size() const
    {
    return plan.getDecoSize(n);
    }
    
QRectF DecoData::boundingRect() const
    {
    return QRectF(0.0, 0.0, plan.getDecoRuntime(), plan.getMaxDepth());
    }

CeilingData::CeilingData(DivePlan& _plan)
    : plan(_plan)
    {
    }
    
QPointF CeilingData::sample(size_t i) const
    {
    return plan.ceilings[i];
    }
    
size_t CeilingData::size() const
    {
    return plan.ceilings.size();
    }
    
QRectF CeilingData::boundingRect() const
    {
    return QRectF(0.0, 0.0, plan.getUserRuntime(), plan.getMaxDepth());
    }

QwtText TimeScaleDraw::label(double t) const
    {
    return QwtText(QString("%1:%2").arg(t/60.0, 0, 'f', 0)
                                   .arg(fmod(t,60.0), 2, 'f', 0, '0'));
    }

class PlotPointPicker : public QwtPlotPicker
    {
    public:
    PlotPointPicker(QWidget* canvas)
        : QwtPlotPicker(QwtPlot::xBottom,
                        QwtPlot::yLeft,
                        QwtPicker::VLineRubberBand,
                        QwtPicker::AlwaysOn,
                        canvas)
        {
        setEnabled(false);
        setStateMachine( new QwtPickerClickPointMachine());
        }

    virtual QwtText trackerTextF(const QPointF &) const;
    };

QwtText PlotPointPicker::trackerTextF(const QPointF& p) const
    {
    qreal t = p.x();
    return QwtText(QString("%1:%2").arg(t/60.0, 0, 'f', 0)
                                   .arg(fmod(t,60.0), 2, 'f', 0, '0'));
    }

DivePlanPlot::DivePlanPlot(DivePlan& _plan, DivePlan& _bailoutPlan, QWidget * parent)
    : QwtPlot(parent), 
      plan(_plan),
      bailoutPlan(_bailoutPlan)
    {
    QwtPlotGrid* grid = new QwtPlotGrid();
    grid->attach(this);
    grid->setPen(QPen(Qt::black, 0, Qt::DotLine));
    setCanvasBackground(Qt::white);
    
    setAxisTitle(QwtPlot::xBottom, tr("[min:s]"));
    axisScaleEngine(QwtPlot::yLeft)->setAttribute(QwtScaleEngine::Inverted);
    setAxisTitle(QwtPlot::yLeft, tr("[m]"));
    
    setAxisScaleDraw(QwtPlot::xBottom, new TimeScaleDraw());
    insertLegend(new QwtLegend(), QwtPlot::BottomLegend);
    
    ceilingCurve = new QwtPlotCurve(tr("Ceiling"));
    ceilingCurve->attach(this);
    ceilingCurve->setPen(QPen(Qt::red, 0, Qt::SolidLine));
    ceilingCurve->setBrush(QBrush(Qt::red, Qt::Dense4Pattern));
    ceilingCurve->setVisible(false);
    
    QwtPlotCurve* maxTissueCurve = new QwtPlotCurve(tr("Max Tissue P"));
    maxTissueCurve->attach(this);
    maxTissueCurve->setPen(QPen(Qt::blue, 0, Qt::SolidLine));
    maxTissueCurve->setSamples(new MaxTissueData(plan, plan.pMaxTissue));
    
    samplesCurve = nullptr;
    
    zoomer = new QwtPlotZoomer( canvas() );
    zoomer->setMousePattern(QwtEventPattern::MouseSelect2,
                            Qt::RightButton, Qt::ControlModifier);
    zoomer->setMousePattern(QwtEventPattern::MouseSelect3,
                            Qt::RightButton, Qt::ShiftModifier);
    connect(zoomer, SIGNAL(zoomed(QRectF)), SLOT(zoomed(QRectF)));

    auto unzoomAction = new QAction("unzoom", this);
    addAction(unzoomAction);
    connect(unzoomAction,SIGNAL(triggered()), SLOT(unzoom()));
    auto fitViewAction = new QAction("Fit view", this);
    addAction(fitViewAction);
    connect(fitViewAction,SIGNAL(triggered()), SLOT(fitView()));
    auto setReturnPointAction = new QAction("Set return point", this);
    addAction(setReturnPointAction);
    connect(setReturnPointAction, SIGNAL(triggered()), SLOT(setReturnPoint()));
    auto setBailoutPointAction = new QAction("Set Bailout point", this);
    addAction(setBailoutPointAction);
    connect(setBailoutPointAction, SIGNAL(triggered()), SLOT(setBailoutPoint()));

    setContextMenuPolicy(Qt::ActionsContextMenu);

    returnPointPicker = new PlotPointPicker(canvas());
    connect(returnPointPicker, SIGNAL(selected(QPointF)),
            SLOT(returnPointSelected(QPointF)));
    colors << Qt::darkRed
           << Qt::darkGreen
           << Qt::darkBlue
           << Qt::darkCyan
           << Qt::darkMagenta
           << Qt::darkYellow;
    ccColor << Qt::black;
    }

void DivePlanPlot::zoomed(const QRectF &r)
    {
    qDebug() << zoomer->zoomStack();
    qDebug() << r;
    }

void DivePlanPlot::unzoom()
    {
    zoomer->zoom(-1);
    }

void DivePlanPlot::fitView()
    {
    zoomer->zoom(0);
    }

void DivePlanPlot::setReturnPoint()
    {
    bailoutPicker = false;
    returnPointPicker->setEnabled(true);
    zoomer->setEnabled(false);
    }

void DivePlanPlot::setBailoutPoint()
    {
    bailoutPicker = true;
    returnPointPicker->setEnabled(true);
    zoomer->setEnabled(false);
    }

void DivePlanPlot::returnPointSelected(const QPointF& pos)
    {
    if (not bailoutPicker)
        {
        plan.createPlanFromSamples(pos.x());
        plan.settings.bailoutRuntime = plan.getUserRuntime() + 2.0;
        }
    else
        plan.settings.bailoutRuntime = pos.x();
    plotPlan();
    returnPointPicker->setEnabled(false);
    zoomer->setEnabled(true);
    }

void DivePlanPlot::plotPlan()
    {
    plot(false);
    }
        
void DivePlanPlot::plot(bool bailout)
    {
    if (plan.ceilings.empty())
        ceilingCurve->setVisible(false);
    else
        {
        ceilingCurve->setVisible(true);
        ceilingCurve->setSamples(new CeilingData(plan));
        }
    
    for (auto curve : decoCurves)
        {
        curve->detach();
        }
    qreal runtime = std::max(plan.getUserRuntime(), plan.getDecoRuntime());
    if (plan.getDecoSeriesSize())
        {
        for (int i = 0; i < plan.getDecoSeriesSize(); ++i)
            {
            const DiveSegment& segment = plan.getDecoSegment(i, 0);
            const Gas& gas = plan.getDecoGas(i);
            QString text = gas.str();
            QColor color;
            if (segment.decoMode == DecoSettings::OPEN_CIRCUIT)
                color = colors[gas.index % colors.size()];
            else
                color = ccColor[gas.index % colors.size()];
            QwtPlotCurve* curve = new QwtPlotCurve(text);
            curve->setRenderHint(QwtPlotItem::RenderAntialiased, true);
            curve->attach(this);
            curve->setPen(QPen(color, 1, Qt::SolidLine));
            curve->setSamples(new DecoData(plan, i));
            decoCurves.append(curve);
            }
        const DiveSegment* lastSegment = nullptr;
        for (const DiveSegment& segment : plan.decoSegments)
            {
            if (segment.type == DiveSegment::STOP && segment.ceiling > 0.0)
                {
                int duration = int(segment.duration / 60.0) + 1.0;
                QString text = QString("%1' %2m").arg(duration).arg(segment.ceiling);
                QwtPlotMarker* marker = new QwtPlotMarker();
                marker->setValue(segment.runtime(), segment.endDepth);
                marker->setLabel(text);
                marker->setLabelAlignment( Qt::AlignLeft | Qt::AlignBottom );
                marker->attach(this);
                decoCurves.append(marker);
                }
            if (lastSegment and lastSegment->type == DiveSegment::GAS_CHANGE)
                {
                const Gas& gas = plan.getGas(segment.gasIndex, segment.decoMode);
                QString text = QString("%1/%2")
                                      .arg(int(gas.oxygen * 100))
                                      .arg(int(gas.helium * 100));
                QwtPlotMarker* marker = new QwtPlotMarker();
                marker->setValue(lastSegment->runtime(), lastSegment->endDepth);
                marker->setLabel(text);
                marker->setLabelAlignment( Qt::AlignLeft | Qt::AlignTop );
                marker->attach(this);
                decoCurves.append(marker);
                }
            lastSegment = &segment;
            }
            
        if (bailout)
            {
            for (int i = 0; i < bailoutPlan.getDecoSeriesSize(); ++i)
                {
                const DiveSegment& segment = bailoutPlan.getDecoSegment(i, 0);
                const Gas& gas = bailoutPlan.getDecoGas(i);
                QString text = "B: ";
                text += gas.str();
                QColor color;
                if (segment.decoMode == DecoSettings::OPEN_CIRCUIT)
                    color = colors[gas.index % colors.size()];
                else
                    color = ccColor[gas.index % colors.size()];
                QwtPlotCurve* curve = new QwtPlotCurve(text);
                curve->setRenderHint(QwtPlotItem::RenderAntialiased, true);
                curve->attach(this);
                curve->setPen(QPen(color, 1, Qt::DashLine));
                curve->setSamples(new DecoData(bailoutPlan, i));
                decoCurves.append(curve);
                }
            runtime = std::max(runtime, bailoutPlan.getDecoRuntime());
            }
        }
    else
        {
        for (int i = 0; i < plan.getPlanSeriesSize(); ++i)
            {
            const Gas& gas = plan.getPlanGas(i);
            QString text = gas.str();
            QwtPlotCurve* curve = new QwtPlotCurve(text);
            curve->setRenderHint(QwtPlotItem::RenderAntialiased, true);
            curve->attach(this);
            curve->setPen(QPen(colors[gas.index % colors.size()], 1, Qt::SolidLine));
            curve->setSamples(new PlanData(plan, i));
            decoCurves.append(curve);
            }
        }
    
    if (plan.samples.size() > 0 and samplesCurve == nullptr)
        {
        samplesCurve = new QwtPlotCurve(tr("Dive samples"));
        samplesCurve->attach(this);
        samplesCurve->setPen(QPen(Qt::gray, 0, Qt::SolidLine));
        samplesCurve->setSamples(new DiveSamplesData(plan));
        }
    if (plan.samples.size() > 0)
        {
        runtime = std::max(runtime, plan.samples.back().time);
        }
    if (plan.samples.size() == 0 and samplesCurve != nullptr)
        {
        samplesCurve->detach();
        }

    if (plan.settings.bailoutRuntime > 0.0)
        {
        auto result = plan.getDepth(plan.settings.bailoutRuntime);
        qreal bailoutDepth = result.get<0>();
        QVector<QPointF> bailoutPoints(2);
        bailoutPoints[0].setX(plan.settings.bailoutRuntime);
        bailoutPoints[0].setY(bailoutDepth -5.0);
        bailoutPoints[1].setX(plan.settings.bailoutRuntime);
        bailoutPoints[1].setY(bailoutDepth +5.0);
        bailoutLine = new QwtPlotCurve();
        bailoutLine->setSamples(bailoutPoints);
        bailoutLine->attach(this);
        bailoutLine->setPen(QPen(Qt::magenta, 2, Qt::SolidLine));
        decoCurves.append(bailoutLine);
        QwtPlotMarker* marker = new QwtPlotMarker();
        marker->setValue(plan.settings.bailoutRuntime, bailoutDepth);
        marker->setLabel(tr("Bailout"));
        marker->setLabelAlignment( Qt::AlignRight | Qt::AlignBottom );
        marker->attach(this);
        decoCurves.append(marker);
        }
    QList<double> ticks = makeTicks(0.0, runtime);
    QwtScaleDiv scaleDiv(0.0, runtime, 
                                    ticks, ticks, ticks);
    setAxisScaleDiv(QwtPlot::xBottom, scaleDiv);
    replot();
    auto xAxis = axisInterval(QwtPlot::xBottom);
    auto yAxis = axisInterval(QwtPlot::yLeft);
    zoomer->setZoomBase(QRectF(xAxis.minValue(),yAxis.minValue(),
                               xAxis.width(),yAxis.width()));

    }
    
PressurePlot::PressurePlot(DivePlan& _plan, QWidget * parent)
    : QwtPlot(parent), 
      plan(_plan)
    {
    QwtPlotGrid* grid = new QwtPlotGrid();
    grid->attach(this);
    grid->setPen(QPen(Qt::black, 0, Qt::DotLine));
    setCanvasBackground(Qt::white);
    
    setAxisTitle(QwtPlot::xBottom, tr("[min:s]"));
    setAxisTitle(QwtPlot::yLeft, tr("[bar]"));
    
    setAxisScaleDraw(QwtPlot::xBottom, new TimeScaleDraw());
    legend = new QwtLegend();
    legend->setDefaultItemMode( QwtLegendData::Checkable );
    insertLegend(legend, QwtPlot::BottomLegend );
    
    oxygenCurve = new QwtPlotCurve(tr("O2"));
    oxygenCurve->setPen(QPen(Qt::green, 0, Qt::SolidLine));
    oxygenCurve->attach(this);
    showCurve(oxygenCurve, true);
    
    nitrogenCurve = new QwtPlotCurve(tr("N2"));
    nitrogenCurve->setPen(QPen(Qt::black, 0, Qt::SolidLine));
    nitrogenCurve->attach(this);
    showCurve(nitrogenCurve, true);
    heliumCurve = new QwtPlotCurve(tr("He"));
    heliumCurve->setPen(QPen(Qt::darkYellow, 0, Qt::SolidLine));
    heliumCurve->attach(this);
    showCurve(heliumCurve, false);
    narcosisCurve = new QwtPlotCurve(tr("N2 + O2"));
    narcosisCurve->setPen(QPen(Qt::red, 0, Qt::SolidLine));
    narcosisCurve->attach(this);
    showCurve(narcosisCurve, false);
    connect(legend, SIGNAL(checked(const QVariant&, bool, int ) ),
        SLOT( showItem(const QVariant&, bool, int ) ) );
    }
    
void PressurePlot::plot()
    {
    QVector<QPointF> data;
    plan.getOxygenList(data);
    oxygenCurve->setSamples(data);
    plan.getNitrogenList(data);
    nitrogenCurve->setSamples(data);
    plan.getHeliumList(data);
    heliumCurve->setSamples(data);
    plan.getNarcosisList(data);
    narcosisCurve->setSamples(data);
    
    qreal runtime = plan.getDecoRuntime();
    QList<double> ticks = makeTicks(0.0, runtime);
    QwtScaleDiv scaleDiv(0.0, runtime, 
                                    ticks, ticks, ticks);
    setAxisScaleDiv(QwtPlot::xBottom, scaleDiv);
    replot();
    }

void PressurePlot::showItem(const QVariant& itemInfo, bool on, int )
    {
    QwtPlotItem* item = itemInfo.value<QwtPlotItem*>();
    showCurve(item, on);
    }
             
void PressurePlot::showCurve( QwtPlotItem *item, bool on )
{
    item->setVisible( on );

    QList<QWidget *> legendWidgets = 
        legend->legendWidgets( itemToInfo( item ) );

    if ( legendWidgets.size() == 1 )
    {
        QwtLegendLabel *legendLabel =
            qobject_cast<QwtLegendLabel *>( legendWidgets[0] );

        if ( legendLabel )
            legendLabel->setChecked( on );
    }

    replot();
}

TissueData::TissueData(DivePlan& _plan, QVector<Tissues>& td, int _n)
    : plan(_plan),
      data(td),
      n(_n)
    {
    }
    
QPointF TissueData::sample(size_t i) const
    {
    return QPointF(i * TISSUE_INTERVAL, data[i][n]);
    }
    
size_t TissueData::size() const
    {
    return data.size();
    }
    
QRectF TissueData::boundingRect() const
    {
    qreal maxP = 0.0;
    for (int j = 0; j < data.size(); ++j)
        {
        maxP = std::max(maxP, data[j][n]);
        }
    return QRectF(0.0, 0.0, data.size() * TISSUE_INTERVAL, 
            maxP);
    }

MaxTissueData::MaxTissueData(DivePlan& _plan, QVector<qreal>& d)
    : plan(_plan),
      data(d)
    {
    }
    
QPointF MaxTissueData::sample(size_t i) const
    {
    return QPointF(i * TISSUE_INTERVAL, 
                   plan.getDepthForPressure(data[i] * DecoSettings::BarToPa));
    }
    
size_t MaxTissueData::size() const
    {
    return data.size();
    }
    
QRectF MaxTissueData::boundingRect() const
    {
    return QRectF(0.0, 0.0, data.size() * TISSUE_INTERVAL, 
            plan.getMaxPressure() * DecoSettings::PaToBar);
    }

TissuePlot::TissuePlot(DivePlan& _plan, QVector<Tissues>& td, QWidget * parent)
    : QwtPlot(parent),
      plan(_plan),
      data(td)
    {
    QwtPlotGrid* grid = new QwtPlotGrid();
    grid->attach(this);
    grid->setPen(QPen(Qt::black, 0, Qt::DotLine));
    setCanvasBackground(Qt::white);
    
    setAxisScaleDraw(QwtPlot::xBottom, new TimeScaleDraw());
    setAxisTitle(QwtPlot::xBottom, tr("[min:s]"));
    setAxisTitle(QwtPlot::yLeft, tr("[bar]"));
    
    for (int i = 0; i < N_TISSUES; ++i)
        {
        QwtPlotCurve* curve = new QwtPlotCurve(tr("N2"));
        curve->setPen(QPen(Qt::black, 0, Qt::SolidLine));
        curve->attach(this);
        curve->setSamples(new TissueData(plan, data, i));
        curves[i] = curve;
        }
    replot();
    }
    
void TissuePlot::plot()
    {
    qreal runtime = plan.getDecoRuntime();
    QList<double> ticks = makeTicks(0.0, runtime);
    QwtScaleDiv scaleDiv(0.0, runtime, 
                                    ticks, ticks, ticks);
    setAxisScaleDiv(QwtPlot::xBottom, scaleDiv);
    replot();
    }
    
