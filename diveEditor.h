//  TauKal: A dive decompression plan software
//  Copyright (C) 2014  Joerg Hunger
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef DIVE_EDITOR_H
#define DIVE_EDITOR_H

#include "diveData.h"

#include <QWidget>

class DiveSegmentModel;
class DivePlanPlot;
class PressurePlot;
class TissuePlot;
class SettingsEditor;
class GasEditor;
class PhoneMenu;

class QTableView;
class QPushButton;
class QBoxLayout;
class QTextEdit;
class QTextDocument;

class DiveEditor : public QWidget
    {
    Q_OBJECT
    
    public:
    
    DiveEditor(QWidget* parent = 0, Qt::WindowFlags f = 0);
    
    signals:
    void calculated();
    
    public slots:
    void decoTypeChanged(DecoSettings::DecoMode);
    void clear();
    void clearDeco();
    void calculate();
    void save();
    void read();
    void readDive();
    
    protected:
    void startUpdate();
    void updateData(bool);
    DecoSettings settings;
    DivePlan plan;
    DivePlan bailoutPlan;
    QTableView* planList;
    DiveSegmentModel* planModel;
    QTextEdit* diveTextView;
    QTextDocument* diveText;
    QTableView* decoList;
    DiveSegmentModel* decoModel;
    QTableView* bailoutList;
    DiveSegmentModel* bailoutModel;
    DivePlanPlot* divePlot;
    PressurePlot* pressurePlot;
    PressurePlot* bailoutPressurePlot;
    SettingsEditor* settingsEditor;
    GasEditor* gasEditor;
    TissuePlot* n2TissuePlot;
    TissuePlot* heTissuePlot;
    QPushButton* clearButton;
    QPushButton* clearDecoButton;
    QPushButton* calcButton;
    QPushButton* readDiveButton;
    int diveCount;
    };

class DesktopDiveEditor : public DiveEditor
    {
    Q_OBJECT
    
    public:
    
    DesktopDiveEditor(Qt::WindowFlags f = 0);
    };
    
class TabletDiveEditor : public DiveEditor
    {
    Q_OBJECT
    
    public:
    
    TabletDiveEditor(Qt::WindowFlags f = 0);
    protected:
    void resizeEvent(QResizeEvent* event);
    private:
    QBoxLayout* planLayout;
    };

class PhoneDiveEditor : public DiveEditor
    {
    Q_OBJECT
    
    public:
    
    PhoneDiveEditor(Qt::WindowFlags f = 0);
    protected slots:
    void setCalculated();
    private:
    int profileIndex;
    PhoneMenu* editorTab;
    };

    
#endif
    
