//  TauKal: A dive decompression plan software
//  Copyright (C) 2014  Joerg Hunger
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "diveModel.h"

#include <QComboBox>
#include <QLineEdit>
#include <QEvent>
#include <QApplication>
#include <cmath>

GasSegmentDelegate::GasSegmentDelegate(DivePlan& _plan, QObject* parent)
    : QItemDelegate(parent),
      plan(_plan)
    {
    }
    
QWidget* GasSegmentDelegate::createEditor(QWidget* parent, 
                                  const QStyleOptionViewItem& , 
                                  const QModelIndex& ) const
    {
    QComboBox* editor = new QComboBox(parent);
    for (int i = 0; i < plan.getGasSize(); ++i)
        {
        if (plan.getGas(i).active)
            editor->addItem(plan.getGas(i).str(), QVariant(i));
        }
    return editor;
    }

void GasSegmentDelegate::setEditorData(QWidget* editor, const QModelIndex& index) const
    {
    QComboBox* comboBox = dynamic_cast<QComboBox*>(editor);
    auto value = index.model()->data(index, Qt::DisplayRole);
    comboBox->setCurrentIndex(comboBox->findText(value.toString()));
    }
    
void GasSegmentDelegate::setModelData(QWidget* editor, 
                              QAbstractItemModel* model, 
                              const QModelIndex& index) const
    {
    QComboBox* comboBox = dynamic_cast<QComboBox*>(editor);
    auto value = comboBox->itemData(comboBox->currentIndex());
    model->setData(index, value);
    }
    

DepthSegmentDelegate::DepthSegmentDelegate(QObject* parent)
    : QItemDelegate(parent)
    {
    }
    
QWidget* DepthSegmentDelegate::createEditor(QWidget* parent, 
                                  const QStyleOptionViewItem& , 
                                  const QModelIndex& ) const
    {
    QLineEdit* editor = new QLineEdit(parent);
    QIntValidator* validator = new QIntValidator(editor);
    validator->setRange(0, 200);
    editor->setValidator(validator);
    editor->setInputMethodHints(Qt::ImhFormattedNumbersOnly);
    QEvent event(QEvent::RequestSoftwareInputPanel);
    QApplication::sendEvent(editor, &event);
    return editor;
    }

void DepthSegmentDelegate::setEditorData(QWidget* editor, const QModelIndex& index) const
    {
    QLineEdit* lineEdit = dynamic_cast<QLineEdit*>(editor);
    auto value = index.model()->data(index, Qt::DisplayRole);
    lineEdit->setText(value.toString());
    }
    
void DepthSegmentDelegate::setModelData(QWidget* editor, 
                              QAbstractItemModel* model, 
                              const QModelIndex& index) const
    {
    QLineEdit* lineEdit = dynamic_cast<QLineEdit*>(editor);
    model->setData(index, QVariant(lineEdit->text()));
    }
    
RuntimeSegmentDelegate::RuntimeSegmentDelegate(QObject* parent)
    : QItemDelegate(parent)
    {
    }
    
QWidget* RuntimeSegmentDelegate::createEditor(QWidget* parent, 
                                  const QStyleOptionViewItem& , 
                                  const QModelIndex& ) const
    {
    QLineEdit* editor = new QLineEdit(parent);
    editor->setInputMask("00:\\0\\0");
    editor->setInputMethodHints(Qt::ImhFormattedNumbersOnly);
    QEvent event(QEvent::RequestSoftwareInputPanel);
    QApplication::sendEvent(editor, &event);
    return editor;
    }

void RuntimeSegmentDelegate::setEditorData(QWidget* editor, const QModelIndex& index) const
    {
    QLineEdit* lineEdit = dynamic_cast<QLineEdit*>(editor);
    auto value = index.model()->data(index, Qt::DisplayRole);
    lineEdit->setText(value.toString());
    }
    
void RuntimeSegmentDelegate::setModelData(QWidget* editor, 
                              QAbstractItemModel* model, 
                              const QModelIndex& index) const
    {
    QLineEdit* lineEdit = dynamic_cast<QLineEdit*>(editor);
    QString text = lineEdit->text().left(lineEdit->text().indexOf(':'));
    model->setData(index, QVariant(text));
    }
    
DiveSegmentModel::DiveSegmentModel(DivePlan& _plan, 
                     QVector<DiveSegment>& _segments, 
                     bool _editable, 
                     QObject* parent)
    : QAbstractTableModel(parent),
      plan(_plan),
      segments(_segments),
      editable(_editable)
    {
    headers << tr("Type")
           << tr("Runtime")
           << tr("Duration")
           << tr("End Depth")
           << tr("Gas");
    if ( ! editable )
        headers << tr("Consumption");
    type2Str << tr("Descent")
             << tr("Ascent")
             << tr("Stop")
             << tr("Problem")
             << tr("Gas Change")
             << tr("Unknown");
    }

void DiveSegmentModel::startUpdate()
    {
    beginResetModel();
    }
    
void DiveSegmentModel::endUpdate()
    {
    endResetModel();
    }
    
int DiveSegmentModel::rowCount(const QModelIndex&) const
    {
    return segments.size();
    }
    
int DiveSegmentModel::columnCount(const QModelIndex&) const
    {
    return headers.size();
    }
    
Qt::ItemFlags DiveSegmentModel::flags(const QModelIndex& index) const
    {
    if (index.isValid() && 
       (index.column() == RUNTIME ||
        index.column() == DURATION ||
        index.column() == END_DEPTH ||
        index.column() == GAS) 
        && editable )
        return  Qt::ItemIsEnabled | Qt::ItemIsEditable;
    return Qt::ItemIsEnabled;
    }
    
QVariant DiveSegmentModel::data(const QModelIndex& index, int role) const
    {
    if (index.isValid() && role == Qt::DisplayRole)
        {
        if (index.column() == TYPE)
            return QVariant(type2Str[segments[index.row()].type]);
        else if (index.column() == RUNTIME)
            return QVariant(timeLabel(segments[index.row()].runtime()));
        else if (index.column() == DURATION)
            return QVariant(timeLabel(segments[index.row()].duration));
        else if (index.column() == START_DEPTH)
            return QVariant(round(segments[index.row()].startDepth));
        else if (index.column() == END_DEPTH)
            return QVariant(round(segments[index.row()].endDepth));
        else if (index.column() == GAS)
            return QVariant(plan.getGas(segments[index.row()].gasIndex,
                                        segments[index.row()].decoMode).str());
        else if (index.column() == CONSUMPTION)
            return QVariant(round(segments[index.row()].consumption));
        }
    return QVariant();
    }
    
QVariant DiveSegmentModel::headerData(int section, Qt::Orientation orientation, 
                                int role) const
    {
    if (role == Qt::DisplayRole)
        {
        if (orientation == Qt::Horizontal)
            return QVariant(headers[section]);
        else
            return QVariant(section + 1);
        }
    return QVariant();
    }        
          
bool DiveSegmentModel::setData(const QModelIndex& dataIndex, const QVariant& value, 
                         int role)
    {
    if (dataIndex.isValid() && role == Qt::EditRole)
        {
        if (dataIndex.column() == RUNTIME)
            {
            qreal duration = value.toInt() * 60.0 
                             - segments[dataIndex.row()].startTime;
            beginResetModel();
            plan.setDuration(dataIndex.row(), duration);
            plan.fixSegments();
            endResetModel();
            return true;
            }
        else if (dataIndex.column() == DURATION)
            {
            qreal duration = value.toInt() * 60.0;
            beginResetModel();
            plan.setDuration(dataIndex.row(), duration);
            plan.fixSegments();
            endResetModel();
            return true;
            }
        else if (dataIndex.column() == END_DEPTH)
            {
            bool ok;
            qreal depth = value.toReal(&ok);
            if (ok)
                {
                beginResetModel();
                plan.setDepth(dataIndex.row(), depth);
                plan.fixSegments();
                endResetModel();
                return true;
                }
            }
        else if (dataIndex.column() == GAS)
            {
            int n = value.toInt();
            segments[dataIndex.row()].gasIndex = n;
            dataChanged(dataIndex, dataIndex);
            return true;
            }
        }
    return false;
    }
    
AscentRateModel::AscentRateModel(QVector<DiveRate>& _rates, 
                     QObject* parent)
    : QAbstractTableModel(parent),
      rates(_rates)
    {
    headers << tr("Depth [m]")
            << tr("Rate [m/min]");
    }
    
int AscentRateModel::rowCount(const QModelIndex&) const
    {
    return rates.size();
    }
    
int AscentRateModel::columnCount(const QModelIndex&) const
    {
    return headers.size();
    }
    
Qt::ItemFlags AscentRateModel::flags(const QModelIndex& index) const
    {
    if (index.isValid())
        return QAbstractTableModel::flags(index) | Qt::ItemIsEditable;
    return Qt::ItemIsEnabled;
    }
    
QVariant AscentRateModel::data(const QModelIndex& index, int role) const
    {
    if (index.isValid() && role == Qt::DisplayRole)
        {
        if (index.column() == DEPTH)
            {
            if (index.row() == 0)
                {
                if (rates.size() == 1)
                    return QVariant(QString("0 -"));
                else
                    return QVariant(QString("0 - %1")
                                .arg(rates[index.row()].depth));
                }
            else if (index.row() == (rates.size() -1))
                return QVariant(QString("%1 - ")
                                .arg(rates[index.row() - 1].depth));
            else
                return QVariant(QString("%1 - %2")
                                .arg(rates[index.row() - 1].depth)
                                .arg(rates[index.row()].depth));
            }
        else if (index.column() == RATE)
            return QVariant(round(rates[index.row()].rate * 60.0));
        }
    return QVariant();
    }
    
QVariant AscentRateModel::headerData(int section, Qt::Orientation orientation, 
                                int role) const
    {
    if (role == Qt::DisplayRole)
        {
        if (orientation == Qt::Horizontal)
            return QVariant(headers[section]);
        else
            return QVariant(section);
        }
    return QVariant();
    }        
          
bool AscentRateModel::setData(const QModelIndex& index, const QVariant& value, 
                         int role)
    {
    if (index.isValid() && role == Qt::EditRole)
        {
        if (index.column() == DEPTH)
            {
            bool ok;
            qreal depth = value.toReal(&ok);
            if (ok)
                {
                rates[index.row()].depth = depth;
                dataChanged(index, index);
                return true;
                }
            }
        else if (index.column() == RATE)
            {
            bool ok;
            qreal rate = value.toReal(&ok);
            if (ok)
                {
                rates[index.row()].rate = rate / 60.0;
                dataChanged(index, index);
                return true;
                }
            }
        }
    return false;
    }
    
void AscentRateModel::addLine()
    {
    beginResetModel();
    rates.append(DiveRate(rates.last().depth, rates.last().rate));
    endResetModel();
    }
    
void AscentRateModel::removeLine()
    {
    if (rates.size() > 1)
        {
        beginResetModel();
        rates.pop_back();
        endResetModel();
        }
    }
    

            
