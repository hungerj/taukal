//  TauKal: A dive decompression plan software
//  Copyright (C) 2014  Joerg Hunger
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "settingsEditor.h"
#include "diveModel.h"

#include <QTableView>
#include <QPushButton>
#include <QLineEdit>
#include <QRadioButton>
#include <QCheckBox>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QGridLayout>
#include <QFormLayout>
#include <QDoubleValidator>
#include <QIcon>
#include <QButtonGroup>
#include <QScrollArea>
#include <QEvent>
#include <QApplication>
#include <QDebug>

#if QT_VERSION >= 0x050000
#include <QScroller>
#include <QScrollerProperties>
#endif

DoubleDelegate::DoubleDelegate(QObject* parent)
    : QItemDelegate(parent)
    {
    }
    
QWidget* DoubleDelegate::createEditor(QWidget* parent, 
                                  const QStyleOptionViewItem& , 
                                  const QModelIndex& ) const
    {
    QLineEdit* editor = new QLineEdit(parent);
    QDoubleValidator* validator = new  QDoubleValidator(editor);
    editor->setValidator(validator);
    editor->setInputMethodHints(Qt::ImhFormattedNumbersOnly);
    QEvent event(QEvent::RequestSoftwareInputPanel);
    QApplication::sendEvent(editor, &event);
    return editor;
    }

void DoubleDelegate::setEditorData(QWidget* editor, const QModelIndex& index) const
    {
    QLineEdit* lineEdit = dynamic_cast<QLineEdit*>(editor);
    auto value = index.model()->data(index, Qt::DisplayRole);
    lineEdit->setText(value.toString());
    }
    
void DoubleDelegate::setModelData(QWidget* editor, 
                              QAbstractItemModel* model, 
                              const QModelIndex& index) const
    {
    QLineEdit* lineEdit = dynamic_cast<QLineEdit*>(editor);
    model->setData(index, QVariant(lineEdit->text()));
    }
    
AscentRateEditor::AscentRateEditor(const QString& title, 
                                   QVector<DiveRate>& rates, 
                                   QWidget * parent)
    : QGroupBox(title, parent)
    {
    QVBoxLayout* widgetLayout = new QVBoxLayout(this);
    
    view = new QTableView(this);
    widgetLayout->addWidget(view);
    model = new AscentRateModel(rates, this);
    view->setModel(model);
    view->setEditTriggers(QAbstractItemView::CurrentChanged);
    view->resizeColumnsToContents();
    
    DoubleDelegate* delegate = new DoubleDelegate(view);
    view->setItemDelegate(delegate);
    
    QHBoxLayout* buttonLayout = new QHBoxLayout();
    widgetLayout->addLayout(buttonLayout);
    
    addButton = new QPushButton(QIcon(":/resource/list-add.png"), 
                                QString(), this);
    buttonLayout->addWidget(addButton);
    removeButton = new QPushButton(QIcon(":/resource/list-remove.png"), 
                                   QString(), this);
    buttonLayout->addWidget(removeButton);
    
    connect(addButton, SIGNAL(clicked()), model, SLOT(addLine()));
    connect(removeButton, SIGNAL(clicked()), model, SLOT(removeLine()));
    int height = view->fontInfo().pixelSize() * 10;
    setMinimumHeight(height);
    }

DesktopSettingsEditor::DesktopSettingsEditor(DecoSettings& _settings, 
                   QWidget* parent, Qt::WindowFlags f)
    : SettingsEditor( _settings, parent, f)
    {
    QGridLayout* widgetLayout = new QGridLayout(this);
    widgetLayout->addWidget(ocButton, 0, 0);
    widgetLayout->addWidget(ccButton, 0, 1);
    widgetLayout->addLayout(leftLayout, 1, 0);
    widgetLayout->addLayout(rightLayout, 1, 1);
        
    widgetLayout->addWidget(descentEditor, 2, 0);
    widgetLayout->addWidget(ascentEditor, 2, 1);
    
    }

PhoneSettingsEditor::PhoneSettingsEditor(DecoSettings& _settings, 
                   QWidget* parent, Qt::WindowFlags f)
    : SettingsEditor( _settings, parent, f)
    {
    QVBoxLayout* widgetLayout = new QVBoxLayout(this);
    QScrollArea* scrollArea = new QScrollArea();
    scrollArea->setWidgetResizable(true);
    widgetLayout->addWidget(scrollArea);
    QWidget* scrollWidget = new QWidget(scrollArea);
    scrollArea->setWidget(scrollWidget);
    QVBoxLayout* scrollLayout = new QVBoxLayout(scrollWidget);
    scrollLayout->setSizeConstraint(QLayout::SetMinimumSize);
    scrollWidget->setMinimumSize(300,300);
    
    QHBoxLayout* buttonLayout = new QHBoxLayout();
    scrollLayout->addLayout(buttonLayout);
    buttonLayout->addWidget(ocButton);
    buttonLayout->addWidget(ccButton);
    scrollLayout->addLayout(leftLayout);
    scrollLayout->addLayout(rightLayout);
    scrollLayout->addWidget(descentEditor);
    scrollLayout->addWidget(ascentEditor);
#if QT_VERSION >= 0x050000
    QScrollerProperties sp;
     
    sp.setScrollMetric(QScrollerProperties::DragVelocitySmoothingFactor, 0.6);
    sp.setScrollMetric(QScrollerProperties::MinimumVelocity, 0.0);
    sp.setScrollMetric(QScrollerProperties::MaximumVelocity, 0.5);
    sp.setScrollMetric(QScrollerProperties::AcceleratingFlickMaximumTime, 0.4);
    sp.setScrollMetric(QScrollerProperties::AcceleratingFlickSpeedupFactor, 1.2);
    sp.setScrollMetric(QScrollerProperties::SnapPositionRatio, 0.2);
    sp.setScrollMetric(QScrollerProperties::MaximumClickThroughVelocity, 0);
    sp.setScrollMetric(QScrollerProperties::DragStartDistance, 0.001);
    sp.setScrollMetric(QScrollerProperties::MousePressEventDelay, 0.5);
     
    QScroller* scroller = QScroller::scroller(scrollArea);
    scroller->grabGesture(scrollArea, QScroller::LeftMouseButtonGesture);
    scroller->setScrollerProperties(sp);

#endif

    }
       
SettingsEditor::SettingsEditor(DecoSettings& _settings, 
                   QWidget* parent, Qt::WindowFlags f)
    : QWidget(parent, f),
      settings(_settings)
    {
    
    QButtonGroup* modeGroup = new QButtonGroup(this);
    
    ocButton = new QRadioButton(tr("Open Circuit"), this);
    modeGroup->addButton(ocButton, DecoSettings::OPEN_CIRCUIT);
    
    ccButton = new QRadioButton(tr("Closed Circuit"), this);
    modeGroup->addButton(ccButton, DecoSettings::CLOSED_CIRCUIT);
    
    roundTimesCheckBox = new QCheckBox(this);
    
    QDoubleValidator* gfValidator = new QDoubleValidator(0.1, 1.2, 2, this);
    QDoubleValidator* po2Validator = new QDoubleValidator(0.1, 2.0, 1, this);
    QDoubleValidator* depthValidator = new
            QDoubleValidator(0.0, 120.0, 1, this);
    QDoubleValidator* factorValidator = new 
            QDoubleValidator(0.0, 10.0, 2, this);
   
    gfLowEditor = new QLineEdit(this);
    gfLowEditor->setValidator(gfValidator);
    
    gfHighEditor = new QLineEdit(this);
    gfHighEditor->setValidator(gfValidator);
    
    bailoutGfLowEditor = new QLineEdit(this);
    bailoutGfLowEditor->setValidator(gfValidator);
    
    bailoutGfHighEditor = new QLineEdit(this);
    bailoutGfHighEditor->setValidator(gfValidator);
    
    surfacePressureEditor = new QLineEdit(this);
    surfacePressureEditor->setValidator(new QIntValidator(100, 1200, this));
    
    salinityEditor = new QLineEdit(this);
    salinityEditor->setValidator(factorValidator);
    
    setPointEditor = new QLineEdit(this);
    setPointEditor->setValidator(po2Validator);
    connect(ccButton, SIGNAL(toggled(bool)), 
            setPointEditor, SLOT(setEnabled(bool)));
    
    lastDecoDepthEditor = new QLineEdit(this);
    lastDecoDepthEditor->setValidator(depthValidator);
    
    decoDistanceEditor = new QLineEdit(this);
    decoDistanceEditor->setValidator(depthValidator);
    
    gasChangeEditor = new QLineEdit(this);
    gasChangeEditor->setValidator(new QIntValidator(0, 300, this));
    
    problemTimeEditor = new QLineEdit(this);
    problemTimeEditor->setValidator(new QIntValidator(0, 300, this));
    
    bailoutFactorEditor = new QLineEdit(this);
    bailoutFactorEditor->setValidator(factorValidator);
    
    bailoutFactorTimeEditor = new QLineEdit(this);
    bailoutFactorTimeEditor->setValidator(new QIntValidator(-1, 3600, this));
    
    caveModeBox = new QCheckBox(this);
    returnFactorEditor = new QLineEdit(this);

    leftLayout = new QFormLayout();
    leftLayout->addRow(tr("Round times"), roundTimesCheckBox);
    leftLayout->addRow(tr("GF Low"), gfLowEditor);
    leftLayout->addRow(tr("GF High"), gfHighEditor);
    leftLayout->addRow(tr("Bailout GF Low"), bailoutGfLowEditor);
    leftLayout->addRow(tr("Bailout GF High"), bailoutGfHighEditor);
    leftLayout->addRow(tr("Surface Pressure [mbar]"), surfacePressureEditor);
    leftLayout->addRow(tr("Density (Salinity) [kg/l]"), salinityEditor);
    leftLayout->addRow(tr("Cave Mode"), caveModeBox);

    rightLayout = new QFormLayout();
    rightLayout->addRow(tr("CCR O2 Set Point [bar]"), setPointEditor);
    rightLayout->addRow(tr("Last Deco Stop Depth [m]"), lastDecoDepthEditor);
    rightLayout->addRow(tr("Distance to Deco Stop [m]"), decoDistanceEditor);
    rightLayout->addRow(tr("Stop time for gas change [s]"), gasChangeEditor);
    rightLayout->addRow(tr("Problem solving time [s]"), problemTimeEditor);
    rightLayout->addRow(tr("Bailout RMV Factor"), bailoutFactorEditor);
    rightLayout->addRow(tr("Bailout RMV Factor Time [s]"), bailoutFactorTimeEditor);
    rightLayout->addRow(tr("Cave return SlowDown Factor"), returnFactorEditor);

    descentEditor = new AscentRateEditor(tr("Descent Rate"), 
                                         settings.descentRates, this);
    ascentEditor = new AscentRateEditor(tr("Ascent Rate"), 
                                         settings.ascentRates, this);
    connect(modeGroup, SIGNAL(buttonClicked(int)), SLOT(modeChanged(int)));
    
    setup();
    }
    
void SettingsEditor::modeChanged(int mode)
    {
    settings.decoType = static_cast<DecoSettings::DecoMode>(mode);
    decoTypeChanged(settings.decoType);
    }

void SettingsEditor::setup()
    {
    ocButton->setChecked(settings.decoType == DecoSettings::OPEN_CIRCUIT);
    ccButton->setChecked(settings.decoType == DecoSettings::CLOSED_CIRCUIT);
    roundTimesCheckBox->setChecked(settings.roundTimes);
    gfLowEditor->setText(QString::number(settings.gfLow, 'f', 2));
    gfHighEditor->setText(QString::number(settings.gfHigh, 'f', 2));
    bailoutGfLowEditor->setText(
        QString::number(settings.bailoutGfLow, 'f', 2));
    bailoutGfHighEditor->setText(
        QString::number(settings.bailoutGfHigh, 'f', 2));
    surfacePressureEditor->setText(
        QString::number(settings.surfacePressure * settings.PaToMBar, 'd', 0));
    salinityEditor->setText(
        QString::number(settings.saltFactor, 'f', 2));
    setPointEditor->setText(QString::number(settings.setPoint, 'f', 1));
    setPointEditor->setEnabled(ccButton->isChecked());
    lastDecoDepthEditor->setText(
        QString::number(settings.lastStopDepth, 'f', 1));
    decoDistanceEditor->setText(
        QString::number(settings.decoStopDistance, 'f', 1));
    gasChangeEditor->setText(
        QString::number(int(settings.gasChangeDuration)));
    problemTimeEditor->setText(
        QString::number(int(settings.bailoutProblemTime)));
    bailoutFactorEditor->setText(
        QString::number(settings.bailoutRmvFactor, 'f', 2));
    bailoutFactorTimeEditor->setText(
        QString::number(int(settings.bailoutRmvFactorTime)));
    caveModeBox->setChecked(settings.caveMode);
    returnFactorEditor->setText(QString::number(settings.returnFactor,'f', 2));
    }
      
void SettingsEditor::apply()
    {
    bool ok;
    qreal value;
    
    settings.roundTimes = roundTimesCheckBox->isChecked();
    
    value = gfLowEditor->text().toDouble(&ok);
    if (ok)
        settings.gfLow = value;
    
    value = gfHighEditor->text().toDouble(&ok);
    if (ok)
        settings.gfHigh = value;
    
    value = bailoutGfLowEditor->text().toDouble(&ok);
    if (ok)
        settings.bailoutGfLow = value;
    
    value = bailoutGfHighEditor->text().toDouble(&ok);
    if (ok)
        settings.bailoutGfHigh = value;
    
    value = surfacePressureEditor->text().toDouble(&ok);
    if (ok)
        settings.surfacePressure = value * settings.MBarToPa;
    
    value = salinityEditor->text().toDouble(&ok);
    if (ok)
        settings.saltFactor = value;
    
    value = setPointEditor->text().toDouble(&ok);
    if (ok)
        settings.setPoint = value;
    
    value = lastDecoDepthEditor->text().toDouble(&ok);
    if (ok)
        settings.lastStopDepth = value;
    
    value = decoDistanceEditor->text().toDouble(&ok);
    if (ok)
        settings.decoStopDistance = value;
    
    value = gasChangeEditor->text().toDouble(&ok);
    if (ok)
        settings.gasChangeDuration = value;
    
    value = problemTimeEditor->text().toDouble(&ok);
    if (ok)
        settings.bailoutProblemTime = value;
    
    value = bailoutFactorEditor->text().toDouble(&ok);
    if (ok)
        settings.bailoutRmvFactor = value;
    
    value = bailoutFactorTimeEditor->text().toDouble(&ok);
    if (ok)
        settings.bailoutRmvFactorTime = value;
    
    settings.caveMode = caveModeBox->isChecked();

    value = returnFactorEditor->text().toDouble(&ok);
    if (ok)
        settings.returnFactor = value;

    }
    

    
    
