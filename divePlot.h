//  TauKal: A dive decompression plan software
//  Copyright (C) 2014  Joerg Hunger
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef DIVEPLOT_H
#define DIVEPLOT_H

#include "diveData.h"

#include <qwt_series_data.h>
#include <qwt_plot.h>
#include <qwt_plot_curve.h>
#include <qwt_scale_draw.h>

class QwtLegend;
class QwtPlotZoomer;
class QwtPlotPicker;

const QList<double> makeTicks(qreal tMin, qreal tMax);

class PlanData : public QwtSeriesData<QPointF>
    {
    public:
    PlanData(DivePlan& plan, int n);
    virtual QPointF sample( size_t i ) const;
    virtual size_t size() const;

    virtual QRectF boundingRect() const;
    private:
    DivePlan& plan;
    int n;
    };

class DiveSamplesData : public QwtSeriesData<QPointF>
    {
    public:
    DiveSamplesData(DivePlan& plan);
    virtual QPointF sample( size_t i ) const;
    virtual size_t size() const;
    virtual QRectF boundingRect() const;

    private:
    DivePlan& plan;
    };

class DecoData : public QwtSeriesData<QPointF>
    {
    public:
    DecoData(DivePlan& plan, int n);
    virtual QPointF sample( size_t i ) const;
    virtual size_t size() const;

    virtual QRectF boundingRect() const;
    private:
    DivePlan& plan;
    int n;
    };

class CeilingData : public QwtSeriesData<QPointF>
    {
    public:
    CeilingData(DivePlan& plan);
    virtual QPointF sample( size_t i ) const;
    virtual size_t size() const;

    virtual QRectF boundingRect() const;
    private:
    DivePlan& plan;
    };

class TimeScaleDraw : public QwtScaleDraw
    {
    virtual QwtText label(double) const;
    };

class DivePlanPlot : public QwtPlot
    {
    Q_OBJECT
    
    public:
    DivePlanPlot(DivePlan& plan, DivePlan& bailoutPlan, QWidget * parent = 0);
    
    
    public slots:
    void plot(bool bailout = true);
    void plotPlan();
    void zoomed(const QRectF&);
    void unzoom();
    void fitView();
    void setReturnPoint();
    void setBailoutPoint();
    void returnPointSelected(const QPointF&);

    protected:
    DivePlan& plan;
    DivePlan& bailoutPlan;
    QVector<QwtPlotItem*> decoCurves;
    QwtPlotCurve* planCurve;
    QwtPlotCurve* ceilingCurve;
    QwtPlotCurve* samplesCurve;
    QVector<QColor> colors;
    QVector<QColor> ccColor;
    QwtPlotZoomer* zoomer;
    QwtPlotPicker* returnPointPicker;
    bool bailoutPicker;
    QwtPlotCurve* bailoutLine;
    };

class PressurePlot : public QwtPlot
    {
    Q_OBJECT
    
    public:
    PressurePlot(DivePlan& plan, QWidget * parent = 0);
    
    
    public slots:
    void plot();
    void showItem(const QVariant& itemInfo, bool on, int index);
    void showCurve( QwtPlotItem *item, bool on );
    
    protected:
    DivePlan& plan;
    QwtPlotCurve* oxygenCurve;
    QwtPlotCurve* nitrogenCurve;
    QwtPlotCurve* heliumCurve;
    QwtPlotCurve* narcosisCurve;
    QwtLegend* legend;
    };

class TissueData : public QwtSeriesData<QPointF>
    {
    public:
    TissueData(DivePlan& plan, QVector<Tissues>&, int n);
    virtual QPointF sample( size_t i ) const;
    virtual size_t size() const;

    virtual QRectF boundingRect() const;
    private:
    DivePlan& plan;
    QVector<Tissues>& data;
    int n;
    };

class MaxTissueData : public QwtSeriesData<QPointF>
    {
    public:
    MaxTissueData(DivePlan& plan, QVector<qreal>&);
    virtual QPointF sample( size_t i ) const;
    virtual size_t size() const;

    virtual QRectF boundingRect() const;
    private:
    DivePlan& plan;
    QVector<qreal>& data;
    };

class TissuePlot : public QwtPlot
    {
    Q_OBJECT
    
    public:
    TissuePlot(DivePlan& plan, QVector<Tissues>&, QWidget * parent = 0);
    public slots:
    void plot();
    
    protected:
    DivePlan& plan;
    QVector<Tissues>& data;
    std::array<QwtPlotCurve*, N_TISSUES> curves;
    };
    
#endif
