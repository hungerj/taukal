//  TauKal: A dive decompression plan software
//  Copyright (C) 2012  Joerg Hunger
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef DECO_CALC_H
#define DECO_CALC_H

#include <QPointF>
#include <QVector>

class DecoSettings;
class DivePlan;
class DecoOSTC;
class Gas;

void applySettings(const DecoSettings& settings,
                   DivePlan& plan,
                   DecoOSTC& deco,
                   bool bailout);

void setGases(QVector<Gas>& gasList, DecoOSTC& deco);

void doDecoCalc(const DecoSettings& settings,
                   DivePlan& plan,
                   bool bailout);
                   
#endif
