//  TauKal: A dive decompression plan software
//  Copyright (C) 2014  Joerg Hunger
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "diveEditor.h"
#include "diveModel.h"
#include "divePlot.h"
#include "gasEditor.h"
#include "settingsEditor.h"
#include "decoCalc.h"
#include "phoneMenu.h"
#include <QTabWidget>
#include <QPushButton>
#include <QSplitter>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QGridLayout>
#include <QTableView>
#include <QDomDocument>
#include <QFile>
#include <QDir>
#include <QTextStream>
#include <QMessageBox>
#include <QResizeEvent>
#include <QScrollArea>
#include <QTextEdit>
#include <QTextDocument>
#include <QFileDialog>

#if TAUKALMOBILE
#include <QScroller>
#include <QScrollerProperties>
#include <QGuiApplication>
#include <QScreen>
#include <QDebug>
#include <QStandardPaths>
#endif

DiveEditor::DiveEditor(QWidget* parent, Qt::WindowFlags f)
    : QWidget(parent, f),
      settings(),
      plan(settings),
      bailoutPlan(settings),
      diveCount(0)
    {
    read();
    planList = new QTableView(this);
    planModel = new DiveSegmentModel(plan, plan.planSegments, true, planList);
    planList->setModel(planModel);
    GasSegmentDelegate* gasDelegate = new GasSegmentDelegate(plan, planList);
    planList->setItemDelegateForColumn(DiveSegmentModel::GAS, gasDelegate);
    DepthSegmentDelegate* depthDelegate = new DepthSegmentDelegate(planList);
    planList->setItemDelegateForColumn(DiveSegmentModel::END_DEPTH, depthDelegate);
    RuntimeSegmentDelegate* timeDelegate = new RuntimeSegmentDelegate(planList);
    planList->setItemDelegateForColumn(DiveSegmentModel::RUNTIME, timeDelegate);
    planList->setItemDelegateForColumn(DiveSegmentModel::DURATION, timeDelegate);
    planList->setEditTriggers(QAbstractItemView::CurrentChanged);
    planList->resizeColumnsToContents();
    planList->setHorizontalScrollMode(QAbstractItemView::ScrollPerPixel);
    
    diveTextView = new QTextEdit(this);
    diveTextView->setReadOnly(true);
    diveText = new QTextDocument(this);
    diveTextView->setDocument(diveText);
    
    decoList = new QTableView(this);
    decoModel = new DiveSegmentModel(plan, plan.decoSegments, false, decoList);
    decoList->setModel(decoModel);
    decoList->resizeColumnsToContents();
    decoList->setHorizontalScrollMode(QAbstractItemView::ScrollPerPixel);
    decoList->setVerticalScrollMode(QAbstractItemView::ScrollPerPixel);
    
    bailoutList = new QTableView(this);
    bailoutModel = new DiveSegmentModel(bailoutPlan, bailoutPlan.decoSegments, false, bailoutList);
    bailoutList->setModel(bailoutModel);
    bailoutList->resizeColumnsToContents();
    bailoutList->setHorizontalScrollMode(QAbstractItemView::ScrollPerPixel);
    bailoutList->setVerticalScrollMode(QAbstractItemView::ScrollPerPixel);
    
#ifdef TAUKALMOBILE
    QScrollerProperties sp;
     
    sp.setScrollMetric(QScrollerProperties::DragVelocitySmoothingFactor, 0.6);
    sp.setScrollMetric(QScrollerProperties::MinimumVelocity, 0.0);
    sp.setScrollMetric(QScrollerProperties::MaximumVelocity, 0.5);
    sp.setScrollMetric(QScrollerProperties::AcceleratingFlickMaximumTime, 0.4);
    sp.setScrollMetric(QScrollerProperties::AcceleratingFlickSpeedupFactor, 1.2);
    sp.setScrollMetric(QScrollerProperties::SnapPositionRatio, 0.2);
    sp.setScrollMetric(QScrollerProperties::MaximumClickThroughVelocity, 0);
    sp.setScrollMetric(QScrollerProperties::DragStartDistance, 0.001);
    sp.setScrollMetric(QScrollerProperties::MousePressEventDelay, 0.5);
     
    QScroller* scroller = QScroller::scroller(planList);
    scroller->grabGesture(planList, QScroller::LeftMouseButtonGesture);
    scroller->setScrollerProperties(sp);

    scroller = QScroller::scroller(diveTextView );
    scroller->grabGesture(diveTextView, QScroller::LeftMouseButtonGesture);
    scroller->setScrollerProperties(sp);

    scroller = QScroller::scroller(decoList);
    scroller->grabGesture(decoList, QScroller::LeftMouseButtonGesture);
    scroller->setScrollerProperties(sp);

    scroller = QScroller::scroller(bailoutList);
    scroller->grabGesture(bailoutList, QScroller::LeftMouseButtonGesture);
    scroller->setScrollerProperties(sp);


    QScreen* screen = QGuiApplication::screens()[0];
    int longSide = std::max(screen->physicalSize().width(), screen->physicalSize().height());
    qDebug() << "longSide: %d" << longSide;
    if (longSide < 150)
        settingsEditor = new PhoneSettingsEditor(settings, this);
    else
        settingsEditor = new DesktopSettingsEditor(settings, this);
    
#else
    settingsEditor = new DesktopSettingsEditor(settings, this);
#endif
    
    gasEditor = new GasEditor(settings, plan, this);
    
    connect(settingsEditor, SIGNAL(decoTypeChanged(DecoSettings::DecoMode)),
            SLOT(decoTypeChanged(DecoSettings::DecoMode)));
    
    clearButton = new QPushButton(tr("Clear"), this);
    connect(clearButton, SIGNAL(clicked()), SLOT(clear()));
    
    clearDecoButton = new QPushButton(tr("Clear Deco"), this);
    connect(clearDecoButton, SIGNAL(clicked()), SLOT(clearDeco()));
    
    readDiveButton = new QPushButton(tr("Read Dive"), this);
    connect(readDiveButton, SIGNAL(clicked()), SLOT(readDive()));
     
    calcButton = new QPushButton(tr("Calculate"), this);
    connect(calcButton, SIGNAL(clicked()), SLOT(calculate()));
    
    divePlot = new DivePlanPlot(plan, bailoutPlan, this);
    
    pressurePlot = new PressurePlot(plan, this);
    pressurePlot->setTitle(tr("Gas Pressures"));
    bailoutPressurePlot = new PressurePlot(bailoutPlan, this);
    bailoutPressurePlot->setTitle(tr("Bailout Pressures"));
    
    n2TissuePlot = new TissuePlot(plan, plan.pN2Tissues, this);
    n2TissuePlot->setTitle(tr("Nitrogen Saturation"));
    heTissuePlot = new TissuePlot(plan, plan.pHeTissues, this);
    heTissuePlot->setTitle(tr("Helium Saturation"));
     
    connect(planModel, SIGNAL(modelReset()),
            divePlot, SLOT(plotPlan()));
    connect(planModel, SIGNAL(modelReset()),
            planList, SLOT(resizeColumnsToContents()));
    }
    
void DiveEditor::decoTypeChanged(DecoSettings::DecoMode mode)
    {
    plan.decoTypeChanged(mode);
    }
    
void DiveEditor::clear()
    {
    startUpdate();
    plan.clear();
    bailoutPlan.clear();
    updateData(false);
    }
    
void DiveEditor::clearDeco()
    {
    startUpdate();
    plan.clearDeco();
    bailoutPlan.clearDeco();
    diveText->clear();
    updateData(false);
    }
    
void DiveEditor::calculate()
    {
    if (plan.planSegments.size() > 1)
        {
        diveCount += 1;
        startUpdate();
        QTextCursor cursor(diveText);
        QTextBlockFormat format;
        QList<QTextOption::Tab> tabs;
        tabs << QTextOption::Tab(200, QTextOption::LeftTab);
        format.setTabPositions(tabs);
        cursor.insertBlock(format);
        QTextCharFormat normal = cursor.charFormat();
        QTextCharFormat bold =  cursor.charFormat();
        bold.setFontWeight(QFont::Bold);
        cursor.setCharFormat(bold);
        cursor.insertText(tr("Dive %1  %3\t%2 m\n").arg(diveCount, 3)
                                                   .arg(plan.getMaxDepth())
                                                   .arg(settings.decoModeStr()));
        settingsEditor->apply();
        plan.clearDeco();
        bailoutPlan.clear();
        bailoutPlan.copy(plan);
        doDecoCalc(settings, plan, false);
        cursor.insertText(tr("Plan\n"));
        cursor.setCharFormat(normal);
        plan.createDivePlanDoc(cursor);
        doDecoCalc(settings, bailoutPlan, true);
        cursor.setCharFormat(bold);
        cursor.insertText(tr("Bailout\n"));
        cursor.setCharFormat(normal);
        bailoutPlan.createDivePlanDoc(cursor);
        updateData(true);
        save();
        }
    }

void DiveEditor::startUpdate()
    {
    planModel->startUpdate();
    decoModel->startUpdate();
    bailoutModel->startUpdate();
    }
    
void DiveEditor::updateData(bool bailout)
    {
    planModel->endUpdate();
    decoModel->endUpdate();
    bailoutModel->endUpdate();
    divePlot->plot(bailout);
    pressurePlot->plot();
    bailoutPressurePlot->plot();
    n2TissuePlot->plot();
    heTissuePlot->plot();
    planList->resizeColumnsToContents();
    decoList->resizeColumnsToContents();
    bailoutList->resizeColumnsToContents();
    emit calculated();
    }
    
void DiveEditor::save()
    {
    QDomDocument doc("TauKalML");
    QDomElement root = doc.createElement("TauKalSettings");
    doc.appendChild(root);
    plan.save(root, doc);
#ifdef TAUKALMOBILE
    QString path = QStandardPaths::writableLocation(QStandardPaths::GenericDataLocation);
    path += "/";
    path += "taukal.ses";
    qDebug() << path;
#else
    QString path = "%1/.taukal.ses";
    path = path.arg(QDir::homePath());
#endif
    QFile settingsFile(path);
    if (!settingsFile.open(QIODevice::WriteOnly))
         return;
    QTextStream settingsStream(&settingsFile);
    doc.save(settingsStream, 2, QDomNode::EncodingFromTextStream);
    settingsFile.close();
    }
    
void DiveEditor::read()
    {
#ifdef TAUKALMOBILE
    QString path = QStandardPaths::writableLocation(QStandardPaths::GenericDataLocation);
    path += "/";
    path += "taukal.ses";
#else
    QString path = "%1/.taukal.ses";
    path = path.arg(QDir::homePath());
#endif
    QFile settingsFile(path);
    if (!settingsFile.open(QIODevice::ReadOnly))
         return;
     
    QString errorStr;
    int errorLine;
    int errorColumn;

    QDomDocument doc;
    if (!doc.setContent(&settingsFile, true, &errorStr, &errorLine,
                                &errorColumn)) {
        QMessageBox::information(window(), tr("DOM Bookmarks"),
                                 tr("Parse error at line %1, column %2:\n%3")
                                 .arg(errorLine)
                                 .arg(errorColumn)
                                 .arg(errorStr));
        }
     else
        {
        QDomElement root = doc.documentElement();
        plan.read(root);
        }
    }

void DiveEditor::readDive()
    {
    QString filePath = QFileDialog::getOpenFileName(this, tr("Open SubSurface File"),
                                                "./",
                                                tr("Dives (*.ssrf)"));
    if ( not filePath.isEmpty())
        {
        QFile diveFile(filePath);
        if (!diveFile.open(QIODevice::ReadOnly))
             return;
        QString errorStr;
        int errorLine;
        int errorColumn;

        QDomDocument doc;
        if (!doc.setContent(&diveFile, true, &errorStr, &errorLine,
                                    &errorColumn)) {
            QMessageBox::information(window(), tr("DOM Bookmarks"),
                                     tr("Parse error at line %1, column %2:\n%3")
                                     .arg(errorLine)
                                     .arg(errorColumn)
                                     .arg(errorStr));
            }
        QDomElement root = doc.documentElement();
        startUpdate();
        plan.readSSRF(root);
        plan.fixSegments();
        updateData(false);
        }
    }
        
DesktopDiveEditor::DesktopDiveEditor(Qt::WindowFlags f)
    : DiveEditor(nullptr, f)
    {
    QVBoxLayout* widgetLayout = new QVBoxLayout(this);
    QSplitter* centralSplitter = new QSplitter(this);
    widgetLayout->addWidget(centralSplitter);
    
    QWidget* editorWidget = new QWidget(this);
    centralSplitter->addWidget(editorWidget);
    centralSplitter->setStretchFactor(0, 1);

    QVBoxLayout* editorLayout = new QVBoxLayout(editorWidget);
    QTabWidget* editorTab = new QTabWidget(editorWidget);
    editorLayout->addWidget(editorTab);
    QSplitter* planSplitter = new QSplitter(Qt::Vertical, this);
    planSplitter->addWidget(planList);
    planSplitter->addWidget(diveTextView);
    
    editorTab->addTab(planSplitter, tr("Plan"));
    editorTab->addTab(decoList, tr("Deco"));
    editorTab->addTab(bailoutList, tr("Bailout"));
    editorTab->addTab(settingsEditor, tr("Settings"));
    editorTab->addTab(gasEditor, tr("Gases"));

    QHBoxLayout* buttonLayout = new QHBoxLayout();
    editorLayout->addLayout(buttonLayout);
    buttonLayout->addWidget(clearButton);
    buttonLayout->addWidget(clearDecoButton);
    buttonLayout->addWidget(readDiveButton);
    buttonLayout->addStretch(1);
    buttonLayout->addWidget(calcButton);
    
    QWidget* plotWidget = new QWidget(this);
    centralSplitter->addWidget(plotWidget);
    centralSplitter->setStretchFactor(1, 2);

    QVBoxLayout* plotLayout = new QVBoxLayout(plotWidget);
    QTabWidget* plotTab = new QTabWidget(plotWidget);
    plotLayout->addWidget(plotTab);
    plotTab->addTab(divePlot, tr("Profile"));
    
    QWidget* pressureWidget = new QWidget(this);
    QVBoxLayout* pressureLayout = new QVBoxLayout(pressureWidget);
    pressureLayout->addWidget(pressurePlot);
    pressureLayout->addWidget(bailoutPressurePlot);
    
    plotTab->addTab(pressureWidget, tr("Pressure"));
    
    QWidget* tissueWidget = new QWidget(this);
    QVBoxLayout* tissueLayout = new QVBoxLayout(tissueWidget);
    tissueLayout->addWidget(n2TissuePlot);
    tissueLayout->addWidget(heTissuePlot);
    
    plotTab->addTab(tissueWidget , tr("Tissues"));
    }

TabletDiveEditor::TabletDiveEditor(Qt::WindowFlags f)
    : DiveEditor(nullptr, f)
    {
    QVBoxLayout* widgetLayout = new QVBoxLayout(this);
    QTabWidget* editorTab = new QTabWidget(this);
    widgetLayout->addWidget(editorTab);
    
    QWidget* planWidget = new QWidget(this);
    planLayout = new QBoxLayout(QBoxLayout::LeftToRight, planWidget);
    QVBoxLayout* textLayout = new QVBoxLayout;
    planLayout->addLayout(textLayout);
    textLayout->addWidget(planList);
    textLayout->addWidget(diveTextView);
    planLayout->addWidget(divePlot);
    
    QWidget* decoWidget = new QWidget(this);
    QHBoxLayout* decoLayout = new QHBoxLayout(decoWidget);
    decoLayout->addWidget(decoList);
    
    QWidget* bailoutWidget = new QWidget(this);
    QHBoxLayout* bailoutLayout = new QHBoxLayout(bailoutWidget);
    bailoutLayout->addWidget(bailoutList);
    
    editorTab->addTab(planWidget, tr("Plan"));
    editorTab->addTab(decoWidget, tr("Deco"));
    editorTab->addTab(bailoutList, tr("Bailout"));
    
    QWidget* pressureWidget = new QWidget(this);
    QGridLayout* pressureLayout = new QGridLayout(pressureWidget);
    pressureLayout->addWidget(pressurePlot, 0 , 0);
    pressureLayout->addWidget(bailoutPressurePlot, 1, 0);
    
    pressureLayout->addWidget(n2TissuePlot, 0, 1);
    pressureLayout->addWidget(heTissuePlot, 1, 1);
    
    editorTab->addTab(pressureWidget , tr("Pressure"));
    editorTab->addTab(settingsEditor, tr("Settings"));
    editorTab->addTab(gasEditor, tr("Gases"));

    QHBoxLayout* buttonLayout = new QHBoxLayout();
    widgetLayout->addLayout(buttonLayout);
    buttonLayout->addWidget(clearButton);
    buttonLayout->addWidget(clearDecoButton);
    buttonLayout->addStretch(1);
    buttonLayout->addWidget(calcButton);
    }
    
void TabletDiveEditor::resizeEvent(QResizeEvent* event)
    {
    if (event->size().width() > event->size().height())
        {
        planLayout->setDirection(QBoxLayout::LeftToRight);
        }
    else
        {
        planLayout->setDirection(QBoxLayout::TopToBottom);
        }
    }
        
PhoneDiveEditor::PhoneDiveEditor(Qt::WindowFlags f)
    : DiveEditor(nullptr, f)
    {
    QVBoxLayout* widgetLayout = new QVBoxLayout(this);
    editorTab = new PhoneMenu(this);
    widgetLayout->addWidget(editorTab);
    
    QWidget* pressureWidget = new QWidget(this);
    QVBoxLayout* areaLayout = new QVBoxLayout(pressureWidget);
    QScrollArea* scrollArea = new QScrollArea();
    scrollArea->setWidgetResizable(true);
    areaLayout->addWidget(scrollArea);
    QWidget* scrollWidget = new QWidget(scrollArea);
    scrollArea->setWidget(scrollWidget);
    QVBoxLayout* pressureLayout = new QVBoxLayout(scrollWidget);
    scrollArea->setWidgetResizable(true);
    pressureLayout->addWidget(pressurePlot);
    pressureLayout->addWidget(bailoutPressurePlot);
    pressureLayout->addWidget(n2TissuePlot);
    pressureLayout->addWidget(heTissuePlot);
    pressureLayout->setSizeConstraint(QLayout::SetMinAndMaxSize);
    
    
#ifdef TAUKALMOBILE
    QScrollerProperties sp;
     
    sp.setScrollMetric(QScrollerProperties::DragVelocitySmoothingFactor, 0.6);
    sp.setScrollMetric(QScrollerProperties::MinimumVelocity, 0.0);
    sp.setScrollMetric(QScrollerProperties::MaximumVelocity, 0.5);
    sp.setScrollMetric(QScrollerProperties::AcceleratingFlickMaximumTime, 0.4);
    sp.setScrollMetric(QScrollerProperties::AcceleratingFlickSpeedupFactor, 1.2);
    sp.setScrollMetric(QScrollerProperties::SnapPositionRatio, 0.2);
    sp.setScrollMetric(QScrollerProperties::MaximumClickThroughVelocity, 0);
    sp.setScrollMetric(QScrollerProperties::DragStartDistance, 0.001);
    sp.setScrollMetric(QScrollerProperties::MousePressEventDelay, 0.5);
     
    QScroller* scroller = QScroller::scroller(scrollArea);
    scroller->grabGesture(scrollArea, QScroller::LeftMouseButtonGesture);
    scroller->setScrollerProperties(sp);

#endif
    QSplitter* planSplitter = new QSplitter(Qt::Vertical, this);
    planSplitter->addWidget(planList);
    planSplitter->addWidget(diveTextView);

    editorTab->addWidget(planSplitter, tr("Plan"));
    profileIndex = editorTab->addWidget(divePlot, tr("Profile"));
    editorTab->addWidget(decoList, tr("Deco"));
    editorTab->addWidget(bailoutList, tr("Bailout"));
    editorTab->addWidget(pressureWidget , tr("Pressure"));
    editorTab->addWidget(settingsEditor, tr("Settings"));
    editorTab->addWidget(gasEditor, tr("Gases"));

    QHBoxLayout* buttonLayout = new QHBoxLayout();
    widgetLayout->addLayout(buttonLayout);
    buttonLayout->addWidget(clearButton);
    buttonLayout->addWidget(clearDecoButton);
    buttonLayout->addStretch(1);
    buttonLayout->addWidget(calcButton);
    editorTab->setCurrent(0);
//    connect(this, SIGNAL(calculated()), SLOT(setCalculated()));
    }
    
void PhoneDiveEditor::setCalculated()
    {
    editorTab->setCurrent(profileIndex);
    }
    
