//  TauKal: A dive decompression plan software
//  Copyright (C) 2014  Joerg Hunger
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "phoneMenu.h"

#include <QStackedWidget>
#include <QMenu>
#include <QLabel>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPushButton>
#include <QSignalMapper>

PhoneMenu::PhoneMenu(QWidget* parent, Qt::WindowFlags f)
    : QWidget(parent, f)
    {
    QVBoxLayout* widgetLayout = new QVBoxLayout(this);
    
    QHBoxLayout* menuLayout = new QHBoxLayout;
    widgetLayout->addLayout(menuLayout);
    
    menuLabel = new QLabel(this);
    menuLayout->addWidget(menuLabel);
    menuLayout->addStretch(1);
    menuButton = new QPushButton(QIcon(":/resource/menuButton-big.png"), QString(), this);
    menuButton->setMinimumSize(60, 60);
    menuLayout->addWidget(menuButton);
    stackWidget = new QStackedWidget(this);
    widgetLayout->addWidget(stackWidget);
    
    connect(menuButton, SIGNAL(clicked()), SLOT(menuClicked()));
    
    mapper = new QSignalMapper(this);
    connect(mapper, SIGNAL(mapped(int)), SLOT(setCurrent(int)));
    
    menu = new QMenu(this);
    }
    
int PhoneMenu::addWidget(QWidget* widget, const QString& text)
    {
    int index = entries.size();
    entries.append(text);
    stackWidget->addWidget(widget);
    QAction* action = new QAction(text, this);
    connect(action, SIGNAL(triggered()), mapper, SLOT(map()));
    mapper->setMapping(action, index);
    menu->addAction(action);
    return index;
    }
    
int PhoneMenu::addWidget(QWidget* widget, const QIcon& icon, const QString& text)
    {
    int index = entries.size();
    entries.append(text);
    stackWidget->addWidget(widget);
    QAction* action = new QAction(icon, text, this);
    connect(action, SIGNAL(triggered()), mapper, SLOT(map()));
    mapper->setMapping(action, index);
    menu->addAction(action);
    return index;
    }
    
void PhoneMenu::setCurrent(int index)
    {
    menuLabel->setText(entries[index]);
    stackWidget->setCurrentIndex(index);
    }
    
void PhoneMenu::menuClicked()
    {
    menu->exec(mapToGlobal(QPoint(width()-menu->width(),0)));
    }
    
    
